<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['absolutebottom'] = 'Absolute onderkant';
$string['absolutemiddle'] = 'Absolute midden';
$string['addblog'] = 'Voeg blog toe';
$string['addpost'] = 'Voeg bericht toe';
$string['alignment'] = 'Uitlijning';
$string['attach'] = 'Bijlage';
$string['attachedfilelistloaded'] = 'Bijgevoegde bestandslijst geladen';
$string['attachedfiles'] = 'Bijgevoegde bestanden';
$string['attachments'] = 'Bijlagen';
$string['baseline'] = 'Basislijn';
$string['blog'] = 'Blog';
$string['blogcopiedfromanotherview'] = 'Opmerking: dit blok is gekopiëerd van een andere pagina. Je kunt het verplaatsen of verwijderen, maar je kunt niet wijzigen wat %s er in zit.';
$string['blogdeleted'] = 'Blog verwijderd';
$string['blogdesc'] = 'Omschrijving';
$string['blogdescdesc'] = 'bv., ‘Een verslag van Janine\'s ervaringen en bevindingen’.';
$string['blogdoesnotexist'] = 'Je probeert toegang te krijgen tot een blog die niet bestaat';
$string['blogfilesdirdescription'] = 'Bestanden geladen als bijlage bij een blogbericht';
$string['blogfilesdirname'] = 'blogbestanden';
$string['blogpost'] = 'Blogbericht';
$string['blogpostdeleted'] = 'Blogbericht verwijderd';
$string['blogpostdoesnotexist'] = 'Je probeert toegang te krijgen tot een blogbericht dat niet bestaat';
$string['blogpostpublished'] = 'Blogbericht gepost';
$string['blogpostsaved'] = 'Blogbericht opgeslagen';
$string['blogs'] = 'Blogs';
$string['blogsettings'] = 'Blog instellingen';
$string['blogtitle'] = 'Titel';
$string['blogtitledesc'] = 'bv., ‘Janine\'s verpleging praktijkhandboek’.';
$string['border'] = 'Randen';
$string['bottom'] = 'Onder';
$string['browsemyfiles'] = 'Blader doorheen mijn bestanden';
$string['cancel'] = 'Annuleren';
$string['cannotdeleteblogpost'] = 'Er is een fout opgetreden bij het verwijderen van dit blogbericht.';
$string['commentsallowed'] = 'Commentaar';
$string['commentsallowedno'] = 'Laat geen commentaar toe op deze blog';
$string['commentsallowedyes'] = 'Laat gebruikers die ingelogd zijn toe commentaar te geven op deze blog';
$string['commentsnotify'] = 'Er is commentaar geplaatst';
$string['commentsnotifydesc'] = 'Optioneel kun je een bericht ontvangen wanneer iemand een commentaar toevoegt aan één van je blog berichten.';
$string['commentsnotifyno'] = 'Breng me niet op de hoogte van commentaar op deze blog';
$string['commentsnotifyyes'] = 'Breng me op de hoogte van commentaar op deze blog';
$string['copyfull'] = 'Anderen zullen hun eigen kopie krijgen van jouw %s';
$string['copynocopy'] = 'Dit blok volledig overslaan wanneer de pagina gekopieëerd wordt';
$string['copyreference'] = 'Anderen kunnen jouw %s tonen op hun pagina';
$string['createandpublishdesc'] = 'Hiermee maakt je een blogbericht beschikbaar voor anderen.';
$string['createasdraftdesc'] = 'Hiermee maak je een blogbericht aan, maar het wordt pas beschikbaar voor anderen wanneer je het publiseert';
$string['createblog'] = 'Maak Blog';
$string['delete'] = 'Verwijderen';
$string['deleteblog?'] = 'Bent je er zeker van dat je deze blog wil verwijderen?';
$string['deleteblogpost?'] = 'Bent je er zeker van dat je dit blogbericht wil verwijderen?';
$string['description'] = 'Beschrijving';
$string['dimensions'] = 'Afmetingen';
$string['draft'] = 'Concept';
$string['edit'] = 'Bewerken';
$string['editblogpost'] = 'Bewerk blogbericht';
$string['erroraccessingblogfilesfolder'] = 'Er is een fout opgetreden bij het benaderen van de map met blogbestanden';
$string['errorsavingattachments'] = 'Er is een fout opgetreden tijdens het opslaan van de bijlage van het blogbericht';
$string['horizontalspace'] = 'Horizontale marge';
$string['insert'] = 'Invoegen';
$string['insertimage'] = 'Afbeelding invoegen';
$string['left'] = 'Links';
$string['middle'] = 'Midden';
$string['moreoptions'] = 'Meer opties';
$string['mustspecifycontent'] = 'Je moet iets van inhoud bepalen voor uw bericht';
$string['mustspecifytitle'] = 'Je moet een titel voor uw bericht bepalen';
$string['myblogs'] = 'Mijn Blogs';
$string['name'] = 'Naam';
$string['newattachmentsexceedquota'] = 'De totale grootte van de nieuwe bestanden die u geladen heeft voor dit bericht zal je quotum overschrijden. Je kan je bericht opslaan als je sommige van de bijlagen die je zojuist hebt toegevoegd verwijderd.';
$string['newblog'] = 'Nieuw Blog';
$string['newblogpost'] = 'Nieuw Blogbericht in Blog "%s"';
$string['nofilesattachedtothispost'] = 'Geen bestanden bijgevoegd';
$string['noimageshavebeenattachedtothispost'] = 'Er zijn geen afbeeldingen bij dit bericht gevoegd. Je zal een afbeelding moeten laden of bijvoegen aan dit bericht voor je het kan invoegen.';
$string['nopostsaddone'] = 'Er zijn nog geen berichten. %sVoeg er een toe%s!';
$string['noresults'] = 'Geen blogberichten gevonden';
$string['pluginname'] = 'Blogs';
$string['postbody'] = 'Berichttekst';
$string['postedbyon'] = 'Gepost door %s op %s';
$string['postedon'] = 'Gepost op';
$string['posts'] = 'berichten';
$string['postscopiedfromview'] = 'Berichten gekopiëerd van %s';
$string['posttitle'] = 'Titel';
$string['publish'] = 'Publiceer';
$string['publishblogpost?'] = 'Ben je er zeker van dat je dit bericht wil publiceren?';
$string['published'] = 'Gepubliceerd';
$string['publishfailed'] = 'Er is een fout opgetreden. Je bericht is niet gepubliceerd';
$string['remove'] = 'Verwijderen';
$string['right'] = 'Rechts';
$string['save'] = 'Opslaan';
$string['saveandpublish'] = 'Opslaan en publiceren';
$string['saveasdraft'] = 'Opslaan als concept';
$string['savepost'] = 'Bericht opslaan';
$string['savesettings'] = 'Instellingen opslaan';
$string['settings'] = 'Instellingen';
$string['texttop'] = 'Tekst bovenkant';
$string['thisisdraft'] = 'Dit bericht is een concept';
$string['thisisdraftdesc'] = 'Als je bericht een concept is kan niemand behalve jij het zien.';
$string['title'] = 'Titel';
$string['top'] = 'Boven';
$string['update'] = 'Bijwerken';
$string['verticalspace'] = 'Verticale marge';
$string['viewblog'] = 'Bekijk blog';
$string['viewposts'] = 'Gekopiëerde berichten (%s)';
$string['youarenottheownerofthisblog'] = 'Je bent niet de eigenaar van deze blog';
$string['youarenottheownerofthisblogpost'] = 'Je bent niet de eigenaar van dit blogbericht';
?>
