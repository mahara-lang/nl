<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['academicgoal'] = 'Onderwijsdoelen';
$string['academicskill'] = 'Onderwijsvaardigheden';
$string['backtoresume'] = 'Terug naar mijn Curriculum Vitae';
$string['book'] = 'Boeken en publicaties';
$string['careergoal'] = 'Werkdoelen';
$string['certification'] = 'Certificaten, erkenningen en prijzen';
$string['citizenship'] = 'Burgerlijke staat';
$string['compositedeleteconfirm'] = 'Bent je er zeker van dat je dit wil verwijderen?';
$string['compositedeleted'] = 'Verwijderen gelukt';
$string['compositesaved'] = 'Opslaan gelukt';
$string['compositesavefailed'] = 'Opslaan mislukt';
$string['contactinformation'] = 'Contactinformatie';
$string['contribution'] = 'Contributie';
$string['coverletter'] = 'Begeleidende brief';
$string['current'] = 'Huidig';
$string['date'] = 'Datum';
$string['dateofbirth'] = 'Geboortedatum';
$string['description'] = 'Omschrijving';
$string['educationhistory'] = 'Onderwijsverleden';
$string['employer'] = 'Werkgever';
$string['employmenthistory'] = 'Loopbaan (werk)';
$string['enddate'] = 'Einddatum';
$string['female'] = 'Vrouw';
$string['gender'] = 'Geslacht';
$string['goalandskillsaved'] = 'Opslaan gelukt';
$string['institution'] = 'Instelling';
$string['interest'] = 'Interesses';
$string['jobdescription'] = 'Functie omschrijving';
$string['jobtitle'] = 'Titel';
$string['male'] = 'Man';
$string['maritalstatus'] = 'Echtelijke staat';
$string['membership'] = 'Professioneel lidmaatschap';
$string['movedown'] = 'Naar beneden';
$string['moveup'] = 'Naar boven';
$string['mygoals'] = 'Mijn doelen';
$string['myresume'] = 'Mijn Curriculum Vitae';
$string['myskills'] = 'Mijn vaardigheden';
$string['personalgoal'] = 'Persoonlijke doelen';
$string['personalinformation'] = 'Persoonlijke informatie';
$string['personalskill'] = 'Persoonlijke vaardigheden';
$string['placeofbirth'] = 'Geboorteplaats';
$string['pluginname'] = 'Curriculum Vitae';
$string['position'] = 'Positie';
$string['qualdescription'] = 'Kwalificatieomschrijving';
$string['qualification'] = 'Kwalificatie';
$string['qualname'] = 'Kwalificatienaam';
$string['qualtype'] = 'Kwalificatietype';
$string['resume'] = 'Curriculum Vitae';
$string['resumesaved'] = 'Curriculum Vitae opgeslagen';
$string['resumesavefailed'] = 'Opslaan van uw Curriculum Vitae is mislukt';
$string['startdate'] = 'Startdatum';
$string['title'] = 'Titel';
$string['visastatus'] = 'Visumstatus';
$string['workskill'] = 'Werk vaardigheden';
?>
