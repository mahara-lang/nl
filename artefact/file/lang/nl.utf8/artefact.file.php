<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Created'] = 'Aangemaakt';
$string['Date'] = 'Datum';
$string['Default'] = 'Standaard';
$string['Description'] = 'Beschrijving';
$string['Download'] = 'Download';
$string['File'] = 'Bestand';
$string['Files'] = 'Bestanden';
$string['Folders'] = 'Mappen';
$string['Name'] = 'Naam';
$string['Owner'] = 'Eigenaar';
$string['Preview'] = 'Voorbeeld';
$string['Size'] = 'Grootte';
$string['Title'] = 'Titel';
$string['Type'] = 'Type';
$string['adminfilesloaded'] = 'Beheerbestanden geladen';
$string['ai'] = 'Postscript-document';
$string['aiff'] = 'AIFF-audiobestand';
$string['application'] = 'Onbekende applicatie';
$string['au'] = 'AU-audiobestand';
$string['avi'] = 'AVI-videobestand';
$string['bmp'] = 'Bitmap-afbeelding';
$string['bytes'] = 'bytes';
$string['bz2'] = 'Bzip2-gecomprimeerd bestand';
$string['cannoteditfolder'] = 'Je hebt geen recht om inhoud toe te voegen aan deze map';
$string['changessaved'] = 'Veranderingen opgeslagen';
$string['contents'] = 'Inhoud';
$string['copyrightnotice'] = 'Copyrightmededeling';
$string['create'] = 'Aanmaken';
$string['createfolder'] = 'Maak nieuwe map aan';
$string['defaultquota'] = 'Standaard quotum';
$string['defaultquotadescription'] = 'Je kunt hier instellen hoeveel schijfruimte beschikbaar is voor nieuwe gebruikers. Bestaande gebruikers quota blijven ongewijzigd.';
$string['deletefile?'] = 'Ben je er zeker van dat je dit bestand wil verwijderen?';
$string['deletefolder?'] = 'Ben je er zeker van dat je deze map wil verwijderen?';
$string['deleteselectedicons'] = 'Verwijder geselecteerde icoontjes';
$string['destination'] = 'Doel';
$string['doc'] = 'MSWord-document';
$string['downloadoriginalversion'] = 'Download de originele versie';
$string['dss'] = 'Digital Speech standaardgeluidsbestand';
$string['editfile'] = 'Bewerk bestand';
$string['editfolder'] = 'Bewerk map';
$string['emptyfolder'] = 'Lege map';
$string['file'] = 'bestand';
$string['filealreadyindestination'] = 'Het bestand dat je wil verplaatsen staat al in deze map';
$string['fileexists'] = 'Bestand bestaat al';
$string['fileexistsonserver'] = 'Een bestand met de naam %s bestaat al.';
$string['fileexistsoverwritecancel'] = 'Een bestand met deze naam bestaat al. Je kunt een andere naam opgeven of het bestaande bestand overschrijven.';
$string['fileinstructions'] = 'Upload je afbeeldingen, documenten of andere bestanden voor weergave in pagina\'s. Om een bestand of map te verplaatsen kun je het verslepen naar de gewenste map.';
$string['filelistloaded'] = 'Bestandslijst geladen';
$string['filemoved'] = 'Bestand succesvol verplaatst';
$string['filenamefieldisrequired'] = 'Het veld bestand is vereist';
$string['files'] = 'bestanden';
$string['filethingdeleted'] = '%s verwijderd';
$string['filetypedescription'] = '<p>Je mag de mogelijke bestanden die gebruikers kunnen uploaden wijzigen. Dit biedt meer controle over wat geüpload kan worden. Deze controle gebeurt naast de standaard viruscontrole als je viruscontrole geactiveerd hebt.</p><p>Merk op dat &quot;onbekende applicaties&quot; nodig kunnen zijn voor sommige video- of archiefbestanden (zoals gzip) om te werken.<p>';
$string['filetypes'] = 'Stel uploadbare bestandstypes in';
$string['flv'] = 'FLV Flash-filmp';
$string['folder'] = 'Map';
$string['foldercreated'] = 'Map aangemaakt';
$string['gif'] = 'GIF-bestand';
$string['groupfiles'] = 'Groepsbestanden';
$string['gz'] = 'Gzip-gecomprimeerd bestand';
$string['home'] = 'Home';
$string['html'] = 'HTML-bestand';
$string['htmlremovedmessage'] = 'U bekijkt <strong>%s</strong> van <a href="%s">%s</a>. Het weergegeven bestand hieronder is gefilterd op mogelijke kwaadwillige inhoud en is een grove weergave van het origineel.';
$string['htmlremovedmessagenoowner'] = 'Je bekijkt <strong>%s</strong>. Het getoonde bestand onderaan is gefilterd om slechte inhoud te verwijderen en het is slechts wat overblijft van het origineel.';
$string['image'] = 'Afbeelding';
$string['imagetitle'] = 'Afbeeldingstitel';
$string['jpeg'] = 'JPEG-afbeelding';
$string['jpg'] = 'JPEG-afbeelding';
$string['js'] = 'Javascript-bestand';
$string['lastmodified'] = 'Laatst bewerkt';
$string['latex'] = 'LaTeX-document';
$string['m3u'] = 'M3U-audiobestand';
$string['mov'] = 'MOV Quicktime-filmp';
$string['movefailed'] = 'Verplaatsen mislukt.';
$string['movefaileddestinationinartefact'] = 'Je kunt een map niet in zichzelf plaatsen.';
$string['movefaileddestinationnotfolder'] = 'Je kunt bestanden alleen naar mappen verplaatsen.';
$string['movefailednotfileartefact'] = 'Alleen bestanden, mappen en afbeeldingsartefacten kunnen verplaatst worden.';
$string['movefailednotowner'] = 'Je hebt geen rechten om een bestand naar deze map te verplaatsen';
$string['mp3'] = 'MP3-audiobestand';
$string['mp4_audio'] = 'MP4-audiobestand';
$string['mp4_video'] = 'MP4-audiobestand';
$string['mpeg'] = 'MPEG-film';
$string['mpg'] = 'MPG-film';
$string['myfiles'] = 'Mijn Bestanden';
$string['namefieldisrequired'] = 'Het veld naam is vereist';
$string['nofilesfound'] = 'Geen bestanden gevonden';
$string['noimagesfound'] = 'Geen afbeeldingen gevonden';
$string['odb'] = 'Openoffice-databank';
$string['odc'] = 'Openoffice Calc-bestand';
$string['odf'] = 'Openoffice Formula-bestand';
$string['odg'] = 'Openoffice Graphics-bestand';
$string['odi'] = 'Openoffice-afbeelding';
$string['odm'] = 'Openoffice Master Document-bestand';
$string['odp'] = 'Openoffice-presentatie';
$string['ods'] = 'Openoffice-rekenblad';
$string['odt'] = 'Openoffice-document';
$string['onlyfiveprofileicons'] = 'Je mag slechts 5 profielicoontjes uploaden';
$string['or'] = 'of';
$string['oth'] = 'Openoffice-webdocument';
$string['ott'] = 'Openoffice-sjabloon';
$string['overwrite'] = 'Overschrijven';
$string['parentfolder'] = 'Bovenliggende hoofdmap';
$string['pdf'] = 'PDF-document';
$string['pluginname'] = 'Bestanden';
$string['png'] = 'PNG-afbeelding';
$string['ppt'] = 'MSPowerpoint-document';
$string['profileicon'] = 'Profiel-icoontje';
$string['profileiconimagetoobig'] = 'De afbeelding die je geüpload hebt is te groot (%s X %s pixels). Ze mag niet groter zijn dan %s X %s pixels';
$string['profileicons'] = 'Profiel-icoontjes';
$string['profileiconsdefaultsetsuccessfully'] = 'Standaard profiel-icoon instellen gelukt';
$string['profileiconsdeletedsuccessfully'] = 'Profiel-icoontjes verwijderen gelukt';
$string['profileiconsetdefaultnotvalid'] = 'Kon het standaard profielicoon niet instellen: het icoon is niet geldig';
$string['profileiconsiconsizenotice'] = 'Je mag hier tot <strong>vijf</strong> profiel-icoontjes uploaden en dan één als standaardicoontje kiezen om te tonen. Je icoontjes moeten tussen 16 X 16 en %s X %s pixels groot zijn.';
$string['profileiconsize'] = 'Grootte profiel-icoontje';
$string['profileiconsnoneselected'] = 'Er zijn geen profiel-icoontjes geselecteerd om te verwijderen';
$string['profileiconuploadexceedsquota'] = 'Door dit profiel-icoontje te uploaden, werd je schijfquotum overschreden. Probeer wat geüploade bestanden te verwijderen';
$string['quicktime'] = 'Quicktime-filmp';
$string['ra'] = 'Real Audio-bestand';
$string['ram'] = 'RAM Real Player-film';
$string['rm'] = 'RM Real Player-film';
$string['rpm'] = 'RPM Real Player-film';
$string['rtf'] = 'RTF-document';
$string['savechanges'] = 'Sla wijzigingen op';
$string['setdefault'] = 'Als standaard instellen';
$string['sgi_movie'] = 'SGI-filmbestand';
$string['sh'] = 'Shellscript';
$string['sitefilesloaded'] = 'Site bestanden geüpload';
$string['swf'] = 'SWF Flash-filmp';
$string['tar'] = 'TAR-archief';
$string['timeouterror'] = 'Bestand uploaden mislukt: probeer het opnieuw';
$string['title'] = 'Naam';
$string['titlefieldisrequired'] = 'Het veld naam is vereist';
$string['txt'] = 'Platte tekstbestand';
$string['unlinkthisfilefromblogposts?'] = 'Dit bestand is gekoppeld aan een of meer blogberichten. Als je het bestand verwijdert zal het ook verwijderd worden uit deze berichten.';
$string['upload'] = 'Uploaden';
$string['uploadedprofileiconsuccessfully'] = 'Nieuw profiel-icoontje uploaden gelukt';
$string['uploadexceedsquota'] = 'Het uploaden van dit bestand zou uw schijfquotum overschrijden. Probeer enkele van uw geüploade bestanden te verwijderen.';
$string['uploadfile'] = 'Bestand uploaden';
$string['uploadfileexistsoverwritecancel'] = 'Een bestand met deze naam bestaat al. Je zou het bestand dat je wil uploaden hernoemen of het bestaande bestand ermee overschrijven.';
$string['uploadingfile'] = 'bestand uploaden ...';
$string['uploadingfiletofolder'] = '%s t/m %s wordt geladen';
$string['uploadoffilecomplete'] = 'Uploaden van %s voltooid';
$string['uploadoffilefailed'] = 'Uploaden van %s mislukt';
$string['uploadoffiletofoldercomplete'] = 'Uploaden van %s t/m %s voltooid';
$string['uploadoffiletofolderfailed'] = 'Uploaden van %s t/m %s mislukt';
$string['uploadprofileicon'] = 'Upload profiel-icoon';
$string['usenodefault'] = 'Geen standaard gebruiken';
$string['usingnodefaultprofileicon'] = 'Nu wordt er geen standaard profiel-icoontje gebruikt';
$string['wav'] = 'WAV-audiobestand';
$string['wmv'] = 'WMV-videobestand';
$string['xml'] = 'XML-bestand';
$string['youmustagreetothecopyrightnotice'] = 'Je moet instemmen met de copyright verklaring';
$string['zip'] = 'ZIP-archief';
?>
