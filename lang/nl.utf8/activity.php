<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addtowatchlist'] = 'Toevoegen aan volglijst';
$string['alltypes'] = 'Alle types';
$string['artefacts'] = 'Artefacts';
$string['attime'] = 'op';
$string['date'] = 'Datum';
$string['deleteallnotifications'] = 'Verwijder alle meldingen';
$string['deletednotifications'] = '%s meldingen verwijderd';
$string['failedtodeletenotifications'] = 'Verwijderen van meldingen mislukt';
$string['failedtomarkasread'] = 'Markeren als gelezen van je meldingen mislukt';
$string['groups'] = 'Groepen';
$string['institutioninvitemessage'] = 'Je kunt je lidmaatschap van dit instituut bevestigen via je Instituut instellingenpagina:';
$string['institutioninvitesubject'] = 'Je bent uitgenodigd om lid te worden van het instituut %s';
$string['institutionrequestmessage'] = 'Je kunt gebruikers toevoegen aan instituten op de Instituten ledenpagina:';
$string['institutionrequestsubject'] = '%s heeft gevraagd om lid te worden van %s';
$string['markasread'] = 'Markeer als gelezen';
$string['markedasread'] = 'Je meldingen zijn als gelezen gemarkeerd';
$string['missingparam'] = 'Vereiste parameter %s was leeg voor activiteitstype %s';
$string['monitored'] = 'Opgevolgd';
$string['newcontactus'] = 'Nieuw contact';
$string['newcontactusfrom'] = 'Nieuw contact van';
$string['newfeedbackonartefact'] = 'Nieuwe feedback op artefact';
$string['newfeedbackonview'] = 'Nieuwe feedback op pagina';
$string['newgroupmembersubj'] = '%s is nu groepslid!';
$string['newviewaccessmessage'] = 'Je bent toegevoegd aan de toegangslijst voor de pagina "%s" van "%s"';
$string['newviewaccesssubject'] = 'Nieuwe paginatoegang';
$string['newviewmessage'] = '%s heeft een nieuwe pagina "%s" gemaakt';
$string['newviewsubject'] = 'Nieuwe pagina gemaakt';
$string['newwatchlistmessage'] = 'Nieuwe activiteit op je volglijst';
$string['newwatchlistmessageview'] = '%s heeft de pagina "%s" gewijzigd';
$string['objectionablecontentartefact'] = 'Aanstootgevende inhoud in artefact "%s" gemeld door %s';
$string['objectionablecontentview'] = 'Aanstootgevende inhoud op pagina "%s" gemeld door %s';
$string['ongroup'] = 'in groep';
$string['ownedby'] = 'is eigendom van';
$string['prefsdescr'] = 'Als je één van de e-mailopties selecteerd, dan zullen meldingen nog steeds in het activiteitenlog terecht komen, maar ze zullen automatisch als gelezen gemarkeerd worden.';
$string['read'] = 'Gelezen';
$string['reallydeleteallnotifications'] = 'Ben je er zeker van dat je al je meldingen wil verwijderen?';
$string['recurseall'] = 'Alle';
$string['removedgroupmembersubj'] = '%s is niet langer lid van de groep';
$string['removefromwatchlist'] = 'Verwijder van volglijst';
$string['selectall'] = 'Alles selecteren';
$string['stopmonitoring'] = 'Stop opvolgen';
$string['stopmonitoringfailed'] = 'Stop opvolgen mislukt';
$string['stopmonitoringsuccess'] = 'Stop opvolgen gelukt';
$string['subject'] = 'Onderwerp';
$string['type'] = 'Activiteitstype';
$string['typeadminmessages'] = 'Beheerdersberichten';
$string['typecontactus'] = 'Contacteer ons';
$string['typefeedback'] = 'Feedback';
$string['typegroupmessage'] = 'Groepsbericht';
$string['typeinstitutionmessage'] = 'Instituutsbericht';
$string['typemaharamessage'] = 'Systeembericht';
$string['typeobjectionable'] = 'Aanstootgevende inhoud';
$string['typeusermessage'] = 'Bericht van andere gebruikers';
$string['typeviewaccess'] = 'Nieuwe paginatoegang';
$string['typevirusrelease'] = 'Virusvlag wegnemen';
$string['typevirusrepeat'] = 'Herhaaldelijk virus uploaden';
$string['typewatchlist'] = 'Volglijst';
$string['unread'] = 'Niet gelezen';
$string['viewmodified'] = 'heeft pagina gewijzigd';
$string['views'] = 'Pagina\'s';
$string['viewsandartefacts'] = 'Pagina\'s en artefacts';
$string['viewsubmittedmessage'] = '%s heeft zijn pagina ingestuurd "%s" aan %s';
$string['viewsubmittedsubject'] = 'Bekijk ingestuurde pagina aan %s';
?>
