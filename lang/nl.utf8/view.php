<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['15,70,15'] = 'Veel bredere middenkolom';
$string['20,30,30,20'] = 'Bredere middenkolommen';
$string['25,25,25,25'] = 'Gelijke breedtes';
$string['25,50,25'] = 'Bredere middenkolom';
$string['33,33,33'] = 'Gelijke breedtes';
$string['33,67'] = 'Bredere rechterkolom';
$string['50,50'] = 'Gelijke breedtes';
$string['67,33'] = 'Bredere linkerkolom';
$string['Added'] = 'Toegevoegd';
$string['Browse'] = 'Bladeren';
$string['Configure'] = 'Configureer';
$string['Owner'] = 'Eigenaar';
$string['Preview'] = 'Voorbeeld';
$string['Search'] = 'Zoeken';
$string['Template'] = 'Sjabloon';
$string['Untitled'] = 'Geen titel';
$string['View'] = 'Pagina';
$string['Views'] = 'Pagina\'s';
$string['access'] = 'Toegang';
$string['accessbetweendates2'] = 'Niemand anders kan deze pagina zien voor %s of na %s';
$string['accessfromdate2'] = 'Niemand anders kan deze pagina zien voor %s';
$string['accessuntildate2'] = 'Niemand anders kan deze pagina zien na %s';
$string['add'] = 'Voeg toe';
$string['addcolumn'] = 'Voeg kolom toe';
$string['addedtowatchlist'] = 'Deze pagina is toegevoegd aan je volglijst';
$string['addfeedbackfailed'] = 'Feedback toevoegen mislukt';
$string['addnewblockhere'] = 'Voeg nieuw blok hier toe';
$string['addtowatchlist'] = 'Voeg pagina toe aan volglijst';
$string['addtutors'] = 'Voeg tutoren toe';
$string['addusertogroup'] = 'Voeg deze gebruiker toe aan een groep';
$string['allowcopying'] = 'Kopiëren toestaan';
$string['allviews'] = 'Alle pagina\'s';
$string['alreadyinwatchlist'] = 'Deze pagina staat al in je volglijst';
$string['artefacts'] = 'Artefacts';
$string['artefactsinthisview'] = 'Artefacts in dit beeld';
$string['attachedfileaddedtofolder'] = 'De bijlage %s is in je map op %s gezet.';
$string['attachfile'] = 'Bestand als bijlage toevoegen';
$string['attachment'] = 'Bijlage';
$string['back'] = 'Terug';
$string['backtocreatemyview'] = 'Terug naar paginaopbouw';
$string['backtoyourview'] = 'Terug naar mijn pagina';
$string['blockcopypermission'] = 'Blokkeer recht op kopiëren';
$string['blockcopypermissiondesc'] = 'Als je andere gebruikers toelaat deze pagina te kopiëren, dan wil je misschien kiezen hoe dit blok gekopiëerd zal worden';
$string['blockinstanceconfiguredsuccessfully'] = 'Blokkeren is geconfigureerd';
$string['blocksinstructionajax'] = 'Sleep blokken onder deze lijn om ze toe te voegen aan je pagina-layout. Je kunt blokken rondslepen in je pagina-layout om ze te positioneren.';
$string['blocksintructionnoajax'] = 'Selecteer een blok en kies waar je het wil toevoegen in je pagina. Je kunt een blok positioneren door de pijltjes in de titelbalk ervan te gebruiken.';
$string['blocktitle'] = 'Bloktitel';
$string['blocktypecategory.feeds'] = 'Externe feeds';
$string['blocktypecategory.fileimagevideo'] = 'Bestanden, afbeeldingen en video';
$string['blocktypecategory.general'] = 'Algemeen';
$string['by'] = 'door';
$string['cantdeleteview'] = 'Je kunt deze pagina niet verwijderen';
$string['canteditdontown'] = 'Je kunt deze pagina niet bewerken omdat jij er niet de eigenaar van bent';
$string['canteditdontownfeedback'] = 'Je kunt deze feedback niet bewerken omdat je er niet de eigenaar van bent';
$string['canteditsubmitted'] = 'Je kunt deze pagina niet bewerken omdat het ingestuurd is voor beoordeling in groep "%s". Je zult moeten wachten tot een tutor je pagina terug vrijgeeft.';
$string['cantsubmitviewtogroup'] = 'Je kunt deze pagina niet insturen voor beoordeling naar deze groep';
$string['changemyviewlayout'] = 'Wijzig Mijn pagina layout';
$string['changeviewlayout'] = 'Wijzig mijn pagina layout';
$string['choosetemplategrouppagedescription'] = '<p>Hier kan je zoeken door de pagina\'s waaruit deze groep mag kopiëren als startpunt om een nieuwe pagina te maken. Je kunt een voorbeeld van elke pagina zien door om de naam er van te klikken. Als je een pagina gevonden hebt die je wil kopiëren, klik dan om de overeenkomstige "Kopiëer pagina"-knop om een kopie te maken en begin het aan te passen.</p><p><strong>Opmerking</strong>Groepen kunnen geen kopies maken van blogs of blogberichten.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Hier kan je zoeken door de pagina\'s waaruit dit instituut mag kopiëren als startpunt om een nieuwe pagina te maken. Je kunt een voorbeeld van elke pagina zien door om de naam er van te klikken. Als je een pagina gevonden hebt die je wil kopiëren, klik dan om de overeenkomstige "Kopiëer pagina"-knop om een kopie te maken en begin het aan te passen.</p><p><strong>Opmerking</strong>Instituten kunnen geen kopies maken van blogs of blogberichten.</p>';
$string['choosetemplatepagedescription'] = '<p>Hier kan je zoeken door de pagina\'s waaruit je mag kopiëren als startpunt om een nieuwe pagina te maken. Je kunt een voorbeeld van elke pagina zien door om de naam er van te klikken. Als je een pagina gevonden hebt die je wil kopiëren, klik dan om de overeenkomstige "Kopiëer pagina"-knop om een kopie te maken en begin het aan te passen.</p>';
$string['clickformoreinformation'] = 'Klik hier voor meer informatie en om feedback te geven';
$string['close'] = 'Sluiten';
$string['complaint'] = 'Klacht';
$string['configureblock'] = 'Configureer dit blok';
$string['confirmcancelcreatingview'] = 'Deze pagina is nog niet afgewerkt. Wil je echt annuleren?';
$string['confirmdeleteblockinstance'] = 'Ben je er zeker van dat je dit blok wil verwijderen?';
$string['copiedblocksandartefactsfromtemplate'] = '%d blokken en %d artefacts gekopiëerd van %s';
$string['copyaview'] = 'Kopiëer een pagina';
$string['copyfornewgroups'] = 'Kopie voor nieuwe groepen';
$string['copyfornewgroupsdescription'] = 'Maak een kopie van deze pagina in alle nieuwe groepen met deze groep types:';
$string['copyfornewmembers'] = 'Kopie voor nieuwe instituutsleden';
$string['copyfornewmembersdescription'] = 'Maak automatisch een persoonlijke kopie van deze pagina voor alle nieuwe leden van %s';
$string['copyfornewusers'] = 'Kopie voor nieuwe gebruikers';
$string['copyfornewusersdescription'] = 'Maak telkens er een nieuwe gebruiker aangemaakt wordt een kopie van deze pagina in zijn portfolio.';
$string['copynewusergroupneedsloggedinaccess'] = 'Pagina\'s die gekopiëerd zijn voor nieuwe gebruikers of groepen moeten toegang geven aan ingelogde gebruikers';
$string['copythisview'] = 'Kopiëer deze pagina';
$string['copyview'] = 'Kopiëer pagina';
$string['createemptyview'] = 'Maak lege pagina';
$string['createtemplate'] = 'Maak sjabloon';
$string['createview'] = 'Maak pagina';
$string['createviewstepone'] = 'Maak pagina stap één: opmaak';
$string['createviewstepthree'] = 'Maak pagina stap drie: toegang';
$string['createviewsteptwo'] = 'Maak pagina stap twee: details';
$string['date'] = 'Datum';
$string['deletespecifiedview'] = 'Verwijder pagina "%s"';
$string['deletethisview'] = 'Verwijder deze pagina';
$string['deleteviewconfirm'] = 'Wil je echt deze pagina verwijderen? Dit kan niet ongedaan gemaakt worden.';
$string['description'] = 'Bekijk beschrijving';
$string['displaymyview'] = 'Toon mijn pagina';
$string['editaccessforview'] = 'Bewerk toegang voor pagina "%s"';
$string['editaccesspagedescription2'] = '<p>Standaard kun jij alleen je pagina zien. Hier kan je kiezen aan wie je nog de informatie in jouw pagina wil tonen. Kies Toevoegen om toegang te verlenen aan Publiek, Ingelogde gebruikers of Vrienden. Gebruik het zoekformulier om individuele groepen of gebruikers toe te voegen. Al wie toegevoegd is, zal in het rechterpaneel verschijnen onder Toegevoegd.</p><p>Je kan anderen ook het recht geven om jouw pagina naar hun eigen portfolio te kopiëren. Wanneer gebruikers een pagina kopiëren, zullen ze automatisch hun eigen kopie van alle bestanden en alle bijhorende mappen in hun portfolio krijgen.</p><p>Als je klaar bent, klik dan onderaan de pagina op Bewaar en ga verder.</p>';
$string['editblocksforview'] = 'Bewerk pagina "%s"';
$string['editblockspagedescription'] = '<p>Kies uit onderstaande tabs de blokken die je wil tonen in je pagina. Je kunt ze slepen in je pagina-opmaak. Selecteer het ?-icoontje voor meer informatie.</p>';
$string['editmyview'] = 'Bewerk mijn pagina';
$string['editprofileview'] = 'Bewerk profielpagina';
$string['editthisview'] = 'Bewerk deze pagina';
$string['editviewaccess'] = 'Bewerk paginatoegang';
$string['editviewdetails'] = 'Bewerk details van pagina "%s"';
$string['editviewnameanddescription'] = 'Bewerk paginadetails';
$string['empty_block'] = 'Kies een artefact uit het linkerpaneel om hier te plaatsen';
$string['emptylabel'] = 'Klik hier om tekst in te geven voor dit label';
$string['err.addblocktype'] = 'Kon het blok niet aan je pagina toevoegen';
$string['err.addcolumn'] = 'Kon nieuwe kolom niet toevoegen';
$string['err.moveblockinstance'] = 'Kon blok niet naar de opgegeven positie verplaatsen';
$string['err.removeblockinstance'] = 'Kon blok niet verwijderen';
$string['err.removecolumn'] = 'Kon kolom niet verwijderen';
$string['everyoneingroup'] = 'Iedereen in de groep';
$string['feedback'] = 'Feedback';
$string['feedbackattachdirdesc'] = 'Bestanden als bijlage bij beoordelingspagina';
$string['feedbackattachdirname'] = 'beoordelingsbestanden';
$string['feedbackattachmessage'] = 'De bijlage is toegevoegd aan je %s map';
$string['feedbackchangedtoprivate'] = 'Feedback naar privé gewijzigd';
$string['feedbackonthisartefactwillbeprivate'] = 'Feedback op dit artefact is alleen zichtbaar voor de eigenaar.';
$string['feedbackonviewbytutorofgroup'] = 'Feedback op %s door %s uit %s';
$string['feedbacksubmitted'] = 'Feedback ingestuurd';
$string['filescopiedfromviewtemplate'] = 'Bestanden gekopiëerd uit %s';
$string['forassessment'] = 'voor beoordeling';
$string['friend'] = 'Vriend';
$string['friends'] = 'Vrienden';
$string['friendslower'] = 'vrienden';
$string['grouplower'] = 'groep';
$string['groups'] = 'Groepen';
$string['groupviews'] = 'Groepspagina\'s';
$string['in'] = 'in';
$string['institutionviews'] = 'Instituutspagina\'s';
$string['invalidcolumn'] = 'Kolom %s buiten bereik';
$string['inviteusertojoingroup'] = 'Nodig deze gebruiker uit voor een groep';
$string['listviews'] = 'Paginalijst';
$string['loggedin'] = 'Ingelogde gebruikers';
$string['loggedinlower'] = 'ingelogde gebruikers';
$string['makeprivate'] = 'Wijzig naar privé';
$string['makepublic'] = 'Maak publiek';
$string['moveblockdown'] = 'Verplaats dit blok lager';
$string['moveblockleft'] = 'Verplaats dit blok naar links';
$string['moveblockright'] = 'Verplaats dit blok naar rechts';
$string['moveblockup'] = 'Verplaats dit blok hoger';
$string['myviews'] = 'Mijn pagina\'s';
$string['next'] = 'Volgende';
$string['noaccesstoview'] = 'Je hebt geen toegang tot deze pagina';
$string['noartefactstochoosefrom'] = 'Geen artefacts gekozen uit';
$string['noblocks'] = 'Geen blokken in deze categorie';
$string['nobodycanseethisview2'] = 'Jij alleen kan deze pagina zien';
$string['nocopyableviewsfound'] = 'Geen pagina\'s om te kopiëren';
$string['noownersfound'] = 'Geen eigenaars gevonden';
$string['nopublicfeedback'] = 'Geen publieke feedback';
$string['notifysiteadministrator'] = 'Verwittig site-beheerder';
$string['notitle'] = 'Geen titel';
$string['noviewlayouts'] = 'Er is geen pagina-opmaak voor een %s kolom pagina';
$string['noviews'] = 'Geen pagina\'s';
$string['numberofcolumns'] = 'Aantal kolommen';
$string['overridingstartstopdate'] = 'Overschrijven start/stop-data';
$string['overridingstartstopdatesdescription'] = 'Als je wil kan je de start en/of einddatum overschrijven. Andere mensen zullen je pagina niet kunnen zien voor de startdatum en na de einddatum ongeacht de toegang die je op andere manieren gegeven hebt.';
$string['owner'] = 'eigenaar';
$string['ownerformat'] = 'Naamopmaak';
$string['ownerformatdescription'] = 'Hoe wil je dat andere mensen die jouw pagina bekijken je naam zien?';
$string['owners'] = 'eigenaars';
$string['placefeedback'] = 'Geef feedback';
$string['placefeedbacknotallowed'] = 'Je hebt het recht niet feedback te geven op deze pagina';
$string['print'] = 'Afdrukken';
$string['profileicon'] = 'Profielafbeelding';
$string['profileviewtitle'] = 'Profielpagina';
$string['public'] = 'Publiek';
$string['publiclower'] = 'publiek';
$string['reallyaddaccesstoemptyview'] = 'Er staan geen blokken op je pagina. Wil je anderen deze pagina echt laten zien?';
$string['removeblock'] = 'Verwijder dit blok';
$string['removecolumn'] = 'Verwijder deze kolom';
$string['removedfromwatchlist'] = 'Deze pagina is verwijderd van je volglijst';
$string['removefromwatchlist'] = 'Verwijder pagina van volglijst';
$string['reportobjectionablematerial'] = 'Rapporteer aanstootgevend materiaal';
$string['reportsent'] = 'Je rapport is verstuurd';
$string['searchowners'] = 'Zoek eigenaars';
$string['searchviews'] = 'Zoek pagina\'s';
$string['searchviewsbyowner'] = 'Zoek pagina\'s per eigenaar:';
$string['selectaviewtocopy'] = 'Selecteer de pagina die je wil kopiëren';
$string['show'] = 'Toon';
$string['startdate'] = 'Startdatum/tijd voor toegang';
$string['startdatemustbebeforestopdate'] = 'De startdatum moet plaatsvinden voor de einddatum';
$string['stopdate'] = 'Einddatum/tijd voor toegang';
$string['submitthisviewto'] = 'Deze pagina insturen naar';
$string['submitviewconfirm'] = 'Als je \'%s\' instuurt naar \'%s\' voor beoordeling, dan zul je het niet kunnen bewerken tot je tutor klaar is met het beoordelen van de pagina. Ben je er zeker van dat je deze pagina nu wil insturen?';
$string['submitviewtogroup'] = 'Stuur \'%s\' in naar \'%s\' voor beoordeling';
$string['success.addblocktype'] = 'Blok toegevoegd';
$string['success.addcolumn'] = 'Kolom toegevoegd';
$string['success.moveblockinstance'] = 'Blok verplaatst';
$string['success.removeblockinstance'] = 'Blok verwijderd';
$string['success.removecolumn'] = 'Kolom verwijderd';
$string['templatedescription'] = 'Selecteer dit als je wil dat wie deze pagina kan zien er ook een kopie kan van maken.';
$string['thisfeedbackisprivate'] = 'Deze feedback is privé';
$string['thisfeedbackispublic'] = 'Deze feedback is publiek';
$string['thisviewmaybecopied'] = 'Kopiëren toegestaan';
$string['title'] = 'Bekijk titel';
$string['token'] = 'Geheime URL';
$string['tutors'] = 'tutoren';
$string['updatewatchlistfailed'] = 'Update van volglijst mislukt';
$string['users'] = 'Gebruikers';
$string['view'] = 'pagina';
$string['viewaccesseditedsuccessfully'] = 'Paginatoegang bewaard';
$string['viewcolumnspagedescription'] = 'Kies eerst het aantal kolommen in je pagina. In de volgende stap zul je de breedte van elke kolom kunnen wijzigen.';
$string['viewcreatedsuccessfully'] = 'Pagina aangemaakt';
$string['viewdeleted'] = 'Pagina verwijderd';
$string['viewfilesdirdesc'] = 'Bestanden van gekopieerde pagina\'s';
$string['viewfilesdirname'] = 'paginabestanden';
$string['viewinformationsaved'] = 'pagina-informatie bewaard';
$string['viewlayoutchanged'] = 'pagina-opmaak gewijzigd';
$string['viewlayoutpagedescription'] = 'Selecteer de opmaak van de kolommen in je pagina';
$string['views'] = 'pagina\'s';
$string['viewsavedsuccessfully'] = 'Pagina bewaard';
$string['viewsby'] = 'Pagina\'s per %s';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Je moet het kopiëren toestaan voor je kunt instellen dat een pagina gekopiëerd kan worden voor nieuwe groepen.';
$string['viewscopiedfornewusersmustbecopyable'] = 'Je moet het kopiëren toestaan voor je kunt instellen dat een pagina gekopiëerd kan worden voor nieuwe gebruikers.';
$string['viewsownedbygroup'] = 'Pagina\'s van deze groep';
$string['viewssharedtogroup'] = 'Pagina\'s gedeeld met deze groep';
$string['viewssharedtogroupbyothers'] = 'Pagina\'s door anderen gedeeld met deze groep';
$string['viewssubmittedtogroup'] = 'Pagina\'s ingestuurd voor deze groep';
$string['viewsubmitted'] = 'Pagina ingestuurd';
$string['viewsubmittedtogroup'] = 'Deze pagina is ingestuurd naar <a href="%sgroup/view.php?id=%s">%s</a>';
$string['watchlistupdated'] = 'Je volglijst is aangepast';
$string['whocanseethisview'] = 'Wie deze pagina kan zien';
$string['youhavenoviews'] = 'Je hebt geen pagina\'s.';
?>
