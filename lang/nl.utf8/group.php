<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['About'] = 'Info';
$string['Admin'] = 'Beheer';
$string['Created'] = 'Aangemaakt';
$string['Files'] = 'Bestanden';
$string['Friends'] = 'Vrienden';
$string['Group'] = 'Groep';
$string['Joined'] = 'Toegevoegd';
$string['Members'] = 'Leden';
$string['Role'] = 'Rol';
$string['Views'] = 'Pagina\'s';
$string['acceptinvitegroup'] = 'Aanvaard';
$string['addedtofriendslistmessage'] = '%s heeft je als vriend toegevoegd! Dit betekent dat %s nu op je vriendenlijst staat. Klik op onderstaande link om hun profielpagina te zien.';
$string['addedtofriendslistsubject'] = 'Nieuwe vriend';
$string['addedtogroupmessage'] = '%s heeft toegevoegd aan een groep, \'%s\'. Klik op onderstaande link om de groep te zien.';
$string['addedtogroupsubject'] = 'Je bent toegevoegd aan een groep';
$string['addnewinteraction'] = 'Voeg nieuwe %s toe';
$string['addtofriendslist'] = 'Toevoegen aan vriendenlijst';
$string['addtomyfriends'] = 'Voeg toe aan vriendenlijst';
$string['adduserfailed'] = 'Gebruiker toevoegen mislukt';
$string['addusertogroup'] = 'Voeg toe aan';
$string['allfriends'] = 'Alle vrienden';
$string['allgroups'] = 'Alle groepen';
$string['allmygroups'] = 'Al mijn groepen';
$string['approve'] = 'Keur goed';
$string['approverequest'] = 'Aanvraag goedkeuren!';
$string['backtofriendslist'] = 'Terug naar vriendenlijst';
$string['cannotinvitetogroup'] = 'Je kunt geen gebruikers uitnodigen voor deze groep';
$string['cannotrequestfriendshipwithself'] = 'Je kunt jezelf niet als vriend uitnodigen';
$string['cannotrequestjoingroup'] = 'Je kunt niet aanvragen om bij deze groep te horen';
$string['cantdeletegroup'] = 'Je kunt deze groep niet verwijderen';
$string['cantdenyrequest'] = 'Dit is geen geldige vriendschapsaanvraag';
$string['canteditdontown'] = 'Je kunt deze groep niet bewerken omdat je er geen eigenaar van bent';
$string['cantleavegroup'] = 'Je kunt deze groep niet verlaten';
$string['cantmessageuser'] = 'Je kunt deze gebruiker geen bericht sturen';
$string['cantremovefriend'] = 'Je kunt deze gebruiker niet van je vriendenlijst schrappen';
$string['cantrequestfriendship'] = 'Je kunt deze gebruiker niet als vriend uitnodigen';
$string['cantrequestfrienship'] = 'Je kunt deze gebruiker niet als vriend uitnodigen';
$string['changerole'] = 'Wijzig rol';
$string['changeroleofuseringroup'] = 'Wijzig rol van %s naar %s';
$string['changeroleto'] = 'Wijzig rol in';
$string['confirmremovefriend'] = 'Ben je er zeker van dat je deze gebruiker van je vriendenlijst wil schrappen?';
$string['couldnotjoingroup'] = 'Je kunt geen lid worden van deze groep';
$string['couldnotleavegroup'] = 'Je kunt deze groep niet verlaten';
$string['couldnotrequestgroup'] = 'Je kunt deze groep geen aanvraag voor lidmaatschap sturen';
$string['creategroup'] = 'Maak groep';
$string['currentfriends'] = 'Huidige vrienden';
$string['currentrole'] = 'Huidige rol';
$string['declineinvitegroup'] = 'Afwijzen';
$string['declinerequest'] = 'Verzoek afwijzen';
$string['deletegroup'] = 'Groep met succes verwijderd';
$string['deleteinteraction'] = 'Verwijder %s \'%s\'';
$string['deleteinteractionsure'] = 'Ben je er zeker van dat je dit wil doen? Het kan niet ongedaan gemaakt worden.';
$string['deletespecifiedgroup'] = 'Verwijder groep \'%s\'';
$string['denyfriendrequest'] = 'Vriendschapsverzoek afwijzen';
$string['denyfriendrequestlower'] = 'Vriendschapsverzoek afwijzen';
$string['denyrequest'] = 'Aanvraag afwijzen';
$string['editgroup'] = 'Bewerk groep';
$string['existingfriend'] = 'bestaande vriend';
$string['findnewfriends'] = 'Zoek nieuwe vrienden';
$string['friend'] = 'vriend';
$string['friendformacceptsuccess'] = 'Aanvaarde vriendschapsverzoeken';
$string['friendformaddsuccess'] = 'Voeg %s toe aan je vriendenlijst';
$string['friendformrejectsuccess'] = 'Geweigerde vriendschapsverzoeken';
$string['friendformremovesuccess'] = '%s van je vriendschapslijst verwijderd';
$string['friendformrequestsuccess'] = 'Stuur een vriendschapsverzoek naar %s';
$string['friendlistfailure'] = 'Wijzigen van je vriendenlijst mislukt';
$string['friendrequestacceptedmessage'] = '%s heeft je vriendschapsverzoek aanvaard en is toegevoegd aan je vriendenlijst';
$string['friendrequestacceptedsubject'] = 'Vriendschapsverzoek aanvaard';
$string['friendrequestrejectedmessage'] = '%s heeft je vriendschapsverzoek afgewezen';
$string['friendrequestrejectedmessagereason'] = '%s heeft je vriendschapsverzoek afgewezen. De reden is:';
$string['friendrequestrejectedsubject'] = 'vriendschapsverzoek afgewezen';
$string['friends'] = 'vrienden';
$string['friendshipalreadyrequested'] = 'Je hebt gevraagd om aan de vriendenlijst van %s toegevoegd te worden';
$string['friendshipalreadyrequestedowner'] = '%s heeft gevraagd om aan je vriendenlijst toegevoegd te worden';
$string['friendshiprequested'] = 'Vriendschap aangevraagd!';
$string['group'] = 'groep';
$string['groupadmins'] = 'Groepsbeheerders';
$string['groupalreadyexists'] = 'Er bestaat al een groep met deze naam';
$string['groupconfirmdelete'] = 'Ben je er zeker van dat je deze groep wil verwijderen?';
$string['groupconfirmdeletehasviews'] = 'Ben je er zeker van dat je deze groep wil verwijderen? Sommige van je pagina\'s gebruiken deze groep voor toegangscontrole. Het verwijderen ervan zou betekenen dat de groepsleden geen toegang meer hebben tot die pagina\'s.';
$string['groupconfirmleave'] = 'Ben je er zeker van dat je deze groep wil verlaten?';
$string['groupconfirmleavehasviews'] = 'Ben je er zeker van dat je deze groep wil verlaten? Sommige van je pagina\'s gebruiken deze groep voor toegangscontrole. Het verwijderen ervan zou betekenen dat de groepsleden geen toegang meer hebben tot die pagina\'s.';
$string['groupdescription'] = 'Groepsbeschrijving';
$string['grouphaveinvite'] = 'Je hebt een uitnodiging voor deze groep gekregen';
$string['grouphaveinvitewithrole'] = 'Je hebt een uitnodiging gekregen om lid te worden van deze groep met als rol';
$string['groupinteractions'] = 'Groepsactiviteiten';
$string['groupinviteaccepted'] = 'Uitnodiging aanvaard! Je bent nu lid van de groep';
$string['groupinvitedeclined'] = 'Uitnodiging afgewezen!';
$string['groupinvitesfrom'] = 'Uitgenodigd:';
$string['groupjointypecontrolled'] = 'Lidmaatschap van deze groep wordt gecontroleerd. Je kunt er niet zomaar lid van worden.';
$string['groupjointypeinvite'] = 'Lidmaatschap van deze groep is enkel op uitnodiging.';
$string['groupjointypeopen'] = 'Deze groep staat open voor nieuwe leden. Je mag er gerust lid van worden!';
$string['groupjointyperequest'] = 'Lidmaatschap van deze groep is enkel op aanvraag.';
$string['groupmemberrequests'] = 'Wachtende aanvragen voor lidmaatschap';
$string['groupmembershipchangedmessageaddedmember'] = 'Je bent toegevoegd als lid van deze groep';
$string['groupmembershipchangedmessageaddedtutor'] = 'Je bent toegevoegd als tutor van deze groep';
$string['groupmembershipchangedmessagedeclinerequest'] = 'Je aanvraag om lid te worden van deze groep is afgewezen';
$string['groupmembershipchangedmessagemember'] = 'Je bent geen tutor meer van deze groep';
$string['groupmembershipchangedmessageremove'] = 'Je bent uit deze groep verwijderd';
$string['groupmembershipchangedmessagetutor'] = 'Je bent tutor geworden van deze groep';
$string['groupmembershipchangesubject'] = 'Groepslidmaatschap: %s';
$string['groupname'] = 'Groepsnaam';
$string['groupnotfound'] = 'Groep met id %s niet gevonden';
$string['groupnotinvited'] = 'Je bent niet uitgenodigd voor deze groep';
$string['grouprequestmessage'] = '%s zou graag lid worden van jouw groep %s';
$string['grouprequestmessagereason'] = '%s zou graag lid worden van jouw groep %s. De reden om lid te worden is: %s';
$string['grouprequestsent'] = 'Aanvraag om lid te worden verstuurd';
$string['grouprequestsubject'] = 'Nieuwe aanvraag voor groepslidmaatschap';
$string['groups'] = 'groepen';
$string['groupsaved'] = 'Groep bewaard';
$string['groupsimin'] = 'Groepen waarvan ik lid ben';
$string['groupsiminvitedto'] = 'Groepen waarvoor ik uitgenodigd ben';
$string['groupsiown'] = 'Groepen waarvan ik eigenaar ben';
$string['groupsiwanttojoin'] = 'Groepen waarvan ik lid wil worden';
$string['groupsnotin'] = 'Groepen waarvan ik geen lid ben';
$string['grouptype'] = 'Groepstype';
$string['hasbeeninvitedtojoin'] = 'is uitgenodigd om lid te worden van deze groep';
$string['hasrequestedmembership'] = 'heeft lidmaatschap voor deze groep aangevraagd';
$string['interactiondeleted'] = '%s verwijderd';
$string['interactionsaved'] = '%s bewaard';
$string['invalidgroup'] = 'De groep bestaat niet';
$string['invite'] = 'Nodig uit';
$string['invitemembertogroup'] = 'Nodig %s uit om lid te worden van \'%s\'';
$string['invitetogroupmessage'] = '%s heeft je uitgenodigd om lid te worden van \'%s\'. Klik op onderstaande link voor meer informatie';
$string['invitetogroupsubject'] = 'Je hebt een uitnodiging voor een groep gekregen';
$string['inviteuserfailed'] = 'Uitnodigen van de gebruiker mislukt';
$string['inviteusertojoingroup'] = 'Uitnodiging voor';
$string['joinedgroup'] = 'Je bent nu lid van de groep';
$string['joingroup'] = 'Word lid van deze groep';
$string['leavegroup'] = 'Verlaat deze groep';
$string['leavespecifiedgroup'] = 'Verlaat groep \'%s\'';
$string['leftgroup'] = 'Je hebt nu deze groep verlaten';
$string['leftgroupfailed'] = 'Verlaten van de groep mislukt';
$string['member'] = 'lid';
$string['memberchangefailed'] = 'Aanpassen van lidmaatschapsinformatie mislukt';
$string['memberchangesuccess'] = 'Status van het lidmaatschap aangepast';
$string['memberrequests'] = 'Aanvragen om lid te worden';
$string['members'] = 'leden';
$string['membershiptype'] = 'Type groepslid';
$string['membershiptype.controlled'] = 'Gecontroleerd lidmaatschap';
$string['membershiptype.invite'] = 'Enkel op uitnodiging';
$string['membershiptype.open'] = 'Open voor iedereen';
$string['membershiptype.request'] = 'Lidmaatschap aanvragen';
$string['memberslist'] = 'Leden:';
$string['messagebody'] = 'Stuur bericht';
$string['messagenotsent'] = 'Bericht verzenden mislukt';
$string['messagesent'] = 'Bericht verzonden!';
$string['newusermessage'] = 'Nieuw bericht van %s';
$string['nobodyawaitsfriendapproval'] = 'Niemand wacht op jouw goedkeuring om je vriend te worden';
$string['nogroups'] = 'Geen groepen';
$string['nogroupsfound'] = 'Geen groepen gevonden :-(';
$string['nointeractions'] = 'Er zijn geen activiteiten in deze groep';
$string['nosearchresultsfound'] = 'Geen zoekresultaten gevonden :-(';
$string['notallowedtodeleteinteractions'] = 'Je hebt het recht niet om activiteiten van deze groep te verwijderen';
$string['notallowedtoeditinteractions'] = 'Je hebt het recht niet om activiteiten van deze groep te bewerken of er toe te voegen';
$string['notamember'] = 'Je bent geen lid van deze groep';
$string['notinanygroups'] = 'In geen enkele groep';
$string['notmembermayjoin'] = 'Je moet lid worden van groep \'%s\' om deze pagina te kunnen zien.';
$string['noviewstosee'] = 'Niets dat jij kunt zien :-(';
$string['pending'] = 'wachtend';
$string['pendingfriends'] = 'Wachtende vrienden';
$string['pendingmembers'] = 'Wachtende leden';
$string['publiclyviewablegroup'] = 'Publiek zichtbare groep?';
$string['publiclyviewablegroupdescription'] = 'Iedereen (inclusief mensen die geen account hebben op deze site) deze groep laten zien? Ook de forums?';
$string['reason'] = 'Reden';
$string['reasonoptional'] = 'Reden (optioneel)';
$string['reject'] = 'Verwerp';
$string['rejectfriendshipreason'] = 'Reden om dit verzoek te verwerpen';
$string['releaseview'] = 'Geef pagina vrij';
$string['remove'] = 'Verwijder';
$string['removedfromfriendslistmessage'] = '%s heeft je van zijn vriendenlijst geschrapt';
$string['removedfromfriendslistmessagereason'] = '%s heeft je van zijn vriendenlijst geschrapt. De reden was:';
$string['removedfromfriendslistsubject'] = 'Verwijderd van vriendenlijst';
$string['removefriend'] = 'Verwijder vriend';
$string['removefromfriends'] = 'Verwijder %s als vriend';
$string['removefromfriendslist'] = 'Verwijder als vriend';
$string['removefromgroup'] = 'Verwijder uit groep';
$string['request'] = 'Verzoek';
$string['requestedfriendlistmessage'] = '%s heeft gevraagd om als vriend toegevoegd te worden. Je kunt dit doen via onderstaande link of vanuit je vriendenlijst.';
$string['requestedfriendlistmessagereason'] = '%s heeft gevraagd om als vriend toegevoegd te worden. Je kunt dit doen via onderstaande link of vanuit je vriendenlijst. De reden is:';
$string['requestedfriendlistsubject'] = 'Nieuw vriendsverzoek';
$string['requestedfriendship'] = 'verzoek vriendschap';
$string['requestedmembershipin'] = 'Verzoek lid worden van:';
$string['requestedtojoin'] = 'Je hebt gevraagd lid te worden van deze groep';
$string['requestfriendship'] = 'Verzoek vriendschap';
$string['requestjoingroup'] = 'Verzoek lidmaatschap van deze groep';
$string['requestjoinspecifiedgroup'] = 'Verzoek lidmaatschap van groep \'%s\'';
$string['rolechanged'] = 'Rol gewijzigd';
$string['savegroup'] = 'Bewaar groep';
$string['seeallviews'] = 'Bekijk alle pagina\'s van %s ...';
$string['sendfriendrequest'] = 'Stuur vriendschapsverzoek';
$string['sendfriendshiprequest'] = 'Stuur %s een vriendschapsverzoek';
$string['sendinvitation'] = 'Stuur uitnodiging';
$string['sendmessage'] = 'Stuur bericht';
$string['sendmessageto'] = 'Stuur bericht aan %s';
$string['submittedviews'] = 'Ingestuurde pagina\'s';
$string['therearependingrequests'] = 'Er zijn %s wachtende leden voor deze groep';
$string['thereispendingrequest'] = 'Er is 1 wachtend lid voor deze groep';
$string['title'] = 'Titel';
$string['trysearchingforfriends'] = 'Probeer %s zoeken naar nieuwe vrienden %s om je netwerk te vergroten!';
$string['trysearchingforgroups'] = 'Probeer %s zoeken naar groepen %s om lid van te worden!';
$string['updatemembership'] = 'Lidmaatschap aanpassen';
$string['user'] = 'gebruiker';
$string['useradded'] = 'Gebruiker toegevoegd';
$string['useralreadyinvitedtogroup'] = 'Deze gebruiker is al uitgenodigd of is al een lid van deze groep.';
$string['usercannotchangetothisrole'] = 'De rol van de gebruiker kan niet in deze rol veranderen';
$string['usercantleavegroup'] = 'De gebruiker kan de groep niet verlaten';
$string['userdoesntwantfriends'] = 'De gebruiker wil geen nieuwe vrienden';
$string['userinvited'] = 'Uitnodiging verstuurd';
$string['userremoved'] = 'Gebruikers verwijderd';
$string['users'] = 'gebruikers';
$string['usersautoadded'] = 'Gebruikers automatisch toegevoegd?';
$string['usersautoaddeddescription'] = 'Automatisch nieuwe gebruikers in deze groep zetten?';
$string['viewreleasedmessage'] = 'De pagina die je ingestuurd had naar groep %s is terug vrijgegeven door %s';
$string['viewreleasedsubject'] = 'Je pagina is vrijgegeven';
$string['viewreleasedsuccess'] = 'Pagina vrijgegeven';
$string['whymakemeyourfriend'] = 'Daarom moet je me als vriend accepteren:';
$string['youaregroupmember'] = 'Je bent lid van deze groep';
$string['youowngroup'] = 'Je bent eigenaar van deze groep';
?>
