<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['accountdeleted'] = 'Je account is verwijderd.';
$string['accountoptionsdesc'] = 'Hier kun je je algemene accountopties instellen';
$string['changepassworddesc'] = 'Als je je wachtwoord wil wijzigen, geef de details dan hier in';
$string['changepasswordotherinterface'] = 'Je kunt je <a href="%s">wachtwoord wijzigen</a> via een andere interface';
$string['changeusername'] = 'Nieuwe gebruikersnaam';
$string['changeusernamedesc'] = 'De gebruikersnaam die je gebruikt om in te loggen bij %s. Gebruikersnamen zijn 3-30 tekens lang en kunnen letters, cijfers en de meeste gewone symbolen bevatten, maar geen spaties.';
$string['changeusernameheading'] = 'Wijzig gebruikersnaam';
$string['deleteaccount'] = 'Verwijder account';
$string['deleteaccountdescription'] = 'Als je je account verwijderd, zal je profielinformatie en je schermen  niet langer meer zichtbaar zijn voor andere gebruikers. De inhoud van forumberichten die je geschreven hebt zal wel zichtbaar blijven, maar de naam van de auteur wordt niet meer getoond.';
$string['friendsauth'] = 'Nieuwe vrienden hebben mijn toelating nodig';
$string['friendsauto'] = 'Nieuwe vrienden worden automatisch toegelaten';
$string['friendsdescr'] = 'Vriendschapscontrole';
$string['friendsnobody'] = 'Niemand mag mij toevoegen als vriend';
$string['language'] = 'Taal';
$string['messagesallow'] = 'Iedereen mag me berichten sturen';
$string['messagesdescr'] = 'Berichten van andere gebruikers';
$string['messagesfriends'] = 'Mijn vrienden mogen me berichten sturen';
$string['messagesnobody'] = 'Niemand mag me berichten sturen';
$string['off'] = 'Uit';
$string['oldpasswordincorrect'] = 'Dit is niet je huidige wachtwoord';
$string['on'] = 'Aan';
$string['prefsnotsaved'] = 'Bewaren van voorkeuren mislukt';
$string['prefssaved'] = 'Voorkeuren bewaard';
$string['showviewcolumns'] = 'Toon menu om kolommen toe te voegen en te verwijderen bij het bewerken van een pagina';
$string['tagssideblockmaxtags'] = 'Maximum aantal tags in de cloud';
$string['tagssideblockmaxtagsdescription'] = 'Maximum aantal tags die in jouw tag cloud getoond mogen worden';
$string['updatedfriendcontrolsetting'] = 'Beheer van vrienden aangepast';
$string['wysiwygdescr'] = 'HTML-editor';
?>
