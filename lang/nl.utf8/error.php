<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['accessdenied'] = 'Toegang geweigerd';
$string['accessdeniedexception'] = 'Je hebt geen toegang tot deze pagina';
$string['artefactnotfound'] = 'Artefact met id %s niet gevonden';
$string['artefactnotfoundmaybedeleted'] = 'Artefact met id %s niet gevonden (misschien is het al verwijderd?)';
$string['artefactpluginmethodmissing'] = 'Artefact plugin %s moet %s implementeren en doet dat niet';
$string['artefacttypeclassmissing'] = 'Artefacttypes moeten een klasse implementeren. %s ontbreekt';
$string['artefacttypemismatch'] = 'Artefacttype fout gekoppeld, je probeert %s te gebruiken als een %s';
$string['artefacttypenametaken'] = 'Artefacttype %s is al ingenomen door een andere plugin (%s)';
$string['blockconfigdatacalledfromset'] = 'Configdata mogen niet rechtstreeks ingegeven worden. Gebruik PluginBlocktype::instance_config_save';
$string['blockinstancednotfound'] = 'Blokinstantie met id %s niet gevonden';
$string['blocktypelibmissing'] = 'Ontbrekende lib.php voor blok %s in artefactplugin %s';
$string['blocktypemissingconfigform'] = 'Bloktype %s moet instance_config_form implementeren';
$string['blocktypenametaken'] = 'Bloktype %s is al ingenomen door een andere plugin (%s)';
$string['blocktypeprovidedbyartefactnotinstallable'] = 'Dit zal geinstalleerd worden als deel van de installatie van artefactplugin %s';
$string['classmissing'] = 'klasse %s voor type %s in plugin %s ontbreekt';
$string['couldnotmakedatadirectories'] = 'Om één of andere reden konden sommige mappen voor hoofdgegevens niet gemaakt worden. Dit zou niet mogen gebeuren, omdat Mahara vroeger al gedetecteerd heeft dat de dataroot-map beschrijfbaar was. Controleer de rechten op de dataroot-map';
$string['curllibrarynotinstalled'] = 'Curl is niet geïnstalleerd op je server. Mahara heeft dat nodig voor de Moodle-integratie en om externe feeds te kunnen ontvangen. Zorg er voor dat curl geladen is in php.ini of installeer het mocht dat nog niet gebeurd zijn.';
$string['datarootinsidedocroot'] = 'Je hebt je dataroot in je wwwroot gemaakt. Dit is een groot beveiligingsprobleem, omdat iedereen die rechtstreeks sessiegegevens aanspreekt (om de sessie van iemand anders te stelen) of bestanden die anderen geüpload hebben en waarvoor zij geen rechten hebben, kunnen bekijken. Configureer je dataroot buiten je wwwroot';
$string['datarootnotwritable'] = 'Je hebt als dataroot %s geconfigureerd en die map is niet beschrijfbaar. Dit betekent dat noch sessiegegevens, nog gebruikersbestanden of andere zaken kunnen geüpload en bewaard worden op je server. Maak de map aan als ze nog niet bestaat of geef de webserver op als eigenaar en gieef die schrijfrechten.';
$string['dbconnfailed'] = 'Mahara kreeg geen verbinding met je applicatiedatabank. Als je een Maharagebruiker bent, wacht dan even en probeer opnieuw. Als je beheerder bent, controleer dan je databankinstellingen en zorg er voor dat je databank beschikbaar is. De fout was:';
$string['dbnotutf8'] = 'Je gebruikt geen UTF-8 databank. Mahara bewaart intern alle gegevens als UTF-8. Drop je databank en maak ze opnieuw aan met UTF-8 encodering.';
$string['dbversioncheckfailed'] = 'Je versie van je databank is te oud voor Mahara. Je versie is %s %s, maar Mahara vereist minstens versie %s.';
$string['gdextensionnotloaded'] = 'Gd is niet geïnstalleerd op je server. Mahara heeft dit nodig om geüploade afbeeldingen te kunnen herschalen. Zorg ervoor dat het geladen wordt via php.ini';
$string['gdfreetypenotloaded'] = 'Freetype-ondersteuning ontbreekt in de serverconfiguratie van de gd-extentie. Mahara heeft dit nodig om CAPTCHA-afbeeldingen te genereren. Zorg ervoor dat je dat mee configureert met gd.';
$string['interactioninstancenotfound'] = 'Activiteit met id %s niet gevonden';
$string['invaliddirection'] = 'Ongeldige richting %s';
$string['invalidviewaction'] = 'Ongeldige pagina controle-actie: %s';
$string['jsonextensionnotloaded'] = 'Je serverconfiguratie mist de JSON-extentie. Mahara heeft dit nodig om gegevens uit te wisselen met de browser. Zorg ervoor dat het geladen wordt via php.ini of installeer de extentie als dat niet gebeurd is.';
$string['magicquotesgpc'] = 'Je hebt gevaarlijke PHP-instellingen, magic_quotes_gpc is ingeschakeld. Mahara zal dit proberen te omzeilen, maar je zou dit echt moeten herstellen.';
$string['magicquotesruntime'] = 'Je hebt gevaarlijke PHP-instellingen, magic_quotes_runtime is ingeschakeld. Mahara zal dit proberen te omzeilen, maar je zou dit echt moeten herstellen.';
$string['magicquotessybase'] = 'Je hebt gevaarlijke PHP-instellingen, magic_quotes_sybase is ingeschakeld. Mahara zal dit proberen te omzeilen, maar je zou dit echt moeten herstellen.';
$string['missingparamblocktype'] = 'Selecteer eerst een bloktype om toe te voegen';
$string['missingparamcolumn'] = 'Kolomspecificatie ontbreekt';
$string['missingparamid'] = 'Id ontbreekt';
$string['missingparamorder'] = 'Volgordespecificatie ontbreekt';
$string['mysqldbextensionnotloaded'] = 'De mysql-extentie ontbreekt in je serverconfiguratie. Mahara heeft dit nodig om gegevens in een relationele databank te kunnen zetten. Zorg er voor dat het geladen is in php.ini of installeer de extentie indien dat nog niet gebeurd is.';
$string['mysqldbtypedeprecated'] = 'You are using the dbtype "mysql" in your config file. Please change it to "mysql5" - "mysql" is deprecated.';
$string['notartefactowner'] = 'Je bent niet de eigenaar van dit artefact';
$string['notfound'] = 'Niet gevonden';
$string['notfoundexception'] = 'De pagina die je zoekt kon niet gevonden worden';
$string['onlyoneprofileviewallowed'] = 'Je hebt slecht recht op één profielpagina';
$string['parameterexception'] = 'Er ontbrak een vereiste parameter';
$string['pgsqldbextensionnotloaded'] = 'De pgsql-extentie ontbreekt in je serverconfiguratie. Mahara heeft dit nodig om gegevens in een relationele databank te kunnen zetten. Zorg er voor dat het geladen is in php.ini of installeer de extentie indien dat nog niet gebeurd is.';
$string['phpversion'] = 'Mahara werkt niet op PHP < %s. Upgrade je PHP-versie of verplaats Mahara naar een andere server.';
$string['registerglobals'] = 'Je hebt gevaarlijke PHP-instellingen, register_globals is ingeschakeld. Mahara zal dit proberen te omzeilen, maar je zou dit echt moeten herstellen.';
$string['safemodeon'] = 'Je server loopt in safe mode. Mahara werkt niet in safe mode. Je moet dit uitschakelen in php.ini of in het configuratiebestand van apache. Als je installatie op shared hosting staat, dan is er waarschijnlijk weinig dat je daar wat kunt aan doen. Vraag het aan je provider of overweeg te verhuizen naar een andere host.';
$string['sessionextensionnotloaded'] = 'De sessie-extentie ontbreekt in je serverconfiguratie. Mahara heeft dit nodig om gegevens het aanmelden van gebruikers mogelijk te maken. Zorg er voor dat het geladen is in php.ini of installeer de extentie indien dat nog niet gebeurd is.';
$string['unknowndbtype'] = 'De serverconfiguratie verwijst naar een onbekend databasetype. Geldige waarden zijn "postgre8" en "mysql5". Gelieve de database instellingen in config.php te wijzigen';
$string['unrecoverableerror'] = 'Er is een onherstelbare fout opgetreden. Dit betekent waarschijnlijk dat je op een bug in het systeem gestoten bent.';
$string['unrecoverableerrortitle'] = '%s - Site niet beschikbaar';
$string['versionphpmissing'] = 'Version.php ontbreekt voor plugin %s %s!';
$string['viewnotfound'] = 'Pagina met id %s niet gevonden';
$string['viewnotfoundexceptionmessage'] = 'Je probeerde een niet-bestaande pagina te bereiken';
$string['viewnotfoundexceptiontitle'] = 'Pagina niet gevonden';
$string['xmlextensionnotloaded'] = 'De extentie %s ontbreekt in je serverconfiguratie. Mahara heeft dit nodig om XML-gegevens te verwerken uit verschillende bronnen. Zorg er voor dat het geladen is in php.ini of installeer de extentie indien dat nog niet gebeurd is.';
?>
