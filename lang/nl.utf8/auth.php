<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addauthority'] = 'Voeg een authoriteit toe';
$string['application'] = 'Applicatie';
$string['authloginmsg'] = 'Toon een boodschap wanneer een gebruiker zich probeert aan te melden via het loginformulier van Mahara';
$string['authname'] = 'Authoriteitsnaam';
$string['cannotremove'] = 'We kunnen deze authenticatieplugin niet verwijderen omdat het de enige plugin is voor dit instituut.';
$string['cannotremoveinuse'] = 'We kunnen deze authenticatieplugin niet verwijdern omdat die gebruikt wordt door sommige gebruikers. Je moet hun records aanpassen voor je deze plugin kunt verwijderen.';
$string['cantretrievekey'] = 'Er is een fout opgetreden tijdens het ophalen van de publieke sleutel van de andere server.<br />Zorg er voor dat de applicatie en de WWW-root velden juist zijn en dat netwerk is ingeschakeld op de andere server.';
$string['changepasswordurl'] = 'Wachtwoord-wijzigings URL';
$string['editauthority'] = 'Bewerk een authoriteit';
$string['errnoauthinstances'] = 'We hebben blijkbaar geen authentificatieplugin geconfigureerd voor de host %s';
$string['errnoxmlrpcinstances'] = 'We hebben blijkbaar geen XMLRPC authentificatieplugin geconfigureerd voor de host %s';
$string['errnoxmlrpcuser'] = 'We konden je nu niet authenticeren. Mogelijke redenen zijn: * je SSO-sessie is verlopen. Ga terug naar de andere applicatie en klik op de link om in te loggen bij Mahara. * Je hebt het recht niet via SSO aan te melden bij Mahara. Vraag het even na bij je beheerder als je denkt dat je wel zou mogen.';
$string['errnoxmlrpcwwwroot'] = 'We hebben geen record voor een host bij %s';
$string['errorcertificateinvalidwwwroot'] = 'Dit certificaat beweert dat het is voor %s, maar je probeert het te gebruiken voor %s';
$string['errorcouldnotgeneratenewsslkey'] = 'Kon geen nieuwe SSL-sleutel aanmaken. Ben je er zeker van dat zowel openssl en de PHP-modele voor openssl op deze server geïnstalleerd zijn?';
$string['errornotvalidsslcertificate'] = 'Dit is geen geldig SSL-certificaat';
$string['host'] = 'Hostnaam of adres';
$string['hostwwwrootinuse'] = 'WWW-root is al in gebruik door een ander instituut (%s)';
$string['ipaddress'] = 'IP-adres';
$string['name'] = 'Site-naam';
$string['noauthpluginconfigoptions'] = 'Er zijn geen configuratie-opties geassocieerd met deze plugin';
$string['nodataforinstance'] = 'Could not find data for auth instance';
$string['parent'] = 'Parent authority';
$string['port'] = 'Poortnummer';
$string['protocol'] = 'Protocol';
$string['requiredfields'] = 'Vereiste profielvelden';
$string['requiredfieldsset'] = 'Vereiste profielvelden ingesteld';
$string['saveinstitutiondetailsfirst'] = 'Bewaar eerst de instituutsdetails voor je de authenticatieplugins configureert.';
$string['shortname'] = 'Korte naam voor jouw site';
$string['ssodirection'] = 'SSO-richting';
$string['theyautocreateusers'] = 'Zij maken gebruikers automatisch aan';
$string['theyssoin'] = 'Zij SSO in';
$string['unabletosigninviasso'] = 'Kon niet inloggen via SSO';
$string['updateuserinfoonlogin'] = 'Update gebruikersinformatie bij login';
$string['updateuserinfoonlogindescription'] = 'Haal de gebruikersinformatie van de andere server en update je lokale gebruikersrecord telkens wanneer de gebruiker inlogt.';
$string['weautocreateusers'] = 'Wij maken gebruikers automatisch aan';
$string['weimportcontent'] = 'Wij importeren inhoud';
$string['weimportcontentdescription'] = '(sommige applicaties)';
$string['wessoout'] = 'Wij SSO uit';
$string['wwwroot'] = 'WWW-root';
$string['xmlrpccouldnotlogyouin'] = 'Sorry, kon je niet inloggen :(';
$string['xmlrpccouldnotlogyouindetail'] = 'We konden je nu niet inloggen bij Mahara. Probeer binnen korte tijd nog eens en als het probleem blijft duren, contacteer dan je beheerder';
$string['xmlrpcserverurl'] = 'XML-RPC Server-URL';
?>
