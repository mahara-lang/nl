<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['All'] = 'Allen';
$string['Artefact'] = 'Artefact';
$string['Artefacts'] = 'Artefacten';
$string['Copyof'] = 'Kopie van %s';
$string['Failed'] = 'Mislukt';
$string['From'] = 'Van';
$string['Permissions'] = 'Rechten';
$string['Query'] = 'Query';
$string['Results'] = 'Resultaten';
$string['Site'] = 'Site';
$string['To'] = 'Aan';
$string['about'] = 'Info';
$string['accept'] = 'Aanvaard';
$string['accessforbiddentoadminsection'] = 'Je hebt onvoldoende rechten om de beheerspagina\'s te zien';
$string['account'] = 'Mijn account';
$string['accountcreated'] = '%s: Nieuwe account';
$string['accountcreatedchangepasswordhtml'] = '<p>Beste %s</p>

<p>Er is voor jou een nieuwe account gemaakt op <a href="%s">%s</a>. Dit zijn je gegevens:</p>

<ul>
    <li><strong>Gebruikersnaam:</strong> %s</li>
    <li><strong>Wachtwoord:</strong> %s</li>
</ul>

<p>Bij je eerste bezoek zal je gevraagd worden om je wachtwoord te wijzigen.</p>

<p>Bezoek <a href="%s">%s</a> om te beginnen!</p>

<p>Vriendelijke groet, %s Site-beheerder</p>';
$string['accountcreatedchangepasswordtext'] = 'Beste %s,

Er is voor jou een nieuwe account gemaakt op %s. Dit zijn je gegevens:

Gebruikersnaam: %s
Wachtwoord: %s

Bij je eerste bezoek zal je gevraagd worden om je wachtwoord te wijzigen.

Bezoek %s om te beginnen!

Vriendelijke groet, %s Site-beheerder';
$string['accountcreatedhtml'] = '<p>Beste %s</p>

<p>Er is voor jou een nieuwe account gemaakt op <a href="%s">%s</a>. Dit zijn je gegevens:</p>

<ul>
    <li><strong>Gebruikersnaam:</strong> %s</li>
    <li><strong>Wachtwoord:</strong> %s</li>
</ul>

<p>Bezoek <a href="%s">%s</a> om te beginnen!</p>

<p>Vriendelijke groet, %s Site-beheerder</p>';
$string['accountcreatedtext'] = 'Beste %s,

Er is voor jou een nieuwe account gemaakt op %s. Dit zijn je gegevens:

Gebruikersnaam: %s
Wachtwoord: %s

Bezoek %s om te beginnen!

Vriendelijke groet, %s Site-beheerder';
$string['accountdeleted'] = 'Je account is verwijderd';
$string['accountexpired'] = 'Je account is verlopen';
$string['accountexpirywarning'] = 'Waarschuwing: account zal verlopen';
$string['accountexpirywarninghtml'] = '<p>Beste %s,</p>
    
<p>Je account op %s zal binnen %s verlopen.</p>

<p>We raden je aan om de inhoud van je portfolio te bewaren via de export-tool. De instructies om deze tool te gebruiken kun je vinden in de gebruikersgids.</p>

<p>Als je je account wil verlengen of je hebt nog vragen,  <a href="%s">contacteer ons dan</a>.</p>

<p>Vriendelijke groet, %s Site-beheerder</p>';
$string['accountexpirywarningtext'] = 'Beste %s,

Je account op %s zal verlopen binnen %s.

We raden je aan om de inhoud van je portfolio te bewaren via de export-tool. De instructies om deze tool te gebruiken kun je vinden in de gebruikersgids.

Als je je account wil verlengen of je hebt nog vragen, contacteer ons dan:

%s

Vriendelijke groet, %s Site-beheerder';
$string['accountinactive'] = 'Je account is niet actief';
$string['accountinactivewarning'] = 'Waarschuwing account uitgeschakeld';
$string['accountinactivewarninghtml'] = '<p>Beste %s,</p>

<p>Je account op %s zal uitgeschakeld worden op %s</p>

<p>Eens uitgeschakeld zal je niet kunnen inloggen tot een beheerder je account terug activeert..</p>

<p>Je kunt het uitschakelen van je account voorkomen door nu aan te melden.</p>

<p>Vriendelijke groet, %s Site-beheerder</p>';
$string['accountinactivewarningtext'] = 'Beste %s,

Je account op %s zal uitgeschakeld worden op %s.

Eens uitgeschakeld zal je niet kunnen inloggen tot een beheerder je account terug activeert.

Je kunt het uitschakelen van je account voorkomen door nu aan te melden.

Vriendelijke groet, %s Site-beheerder';
$string['accountprefs'] = 'Voorkeuren';
$string['accountsuspended'] = 'Je account is uitgeschakeld vanaf %s. Reden:<blockquote>%s</blockquote>';
$string['activityprefs'] = 'Activiteitsvoorkeuren';
$string['add'] = 'Voeg toe';
$string['addemail'] = 'E-mailadres toevoegen';
$string['adminphpuploaderror'] = 'Een bestandsuploadfout is mogelijk veroorzaakt door je serverconfiguratie.';
$string['allowpublicaccess'] = 'Publieke toegang (zonder inloggen) toelaten';
$string['allusers'] = 'Alle gebruikers';
$string['alphabet'] = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
$string['artefact'] = 'artefact';
$string['artefactnotfound'] = 'Artefact met id %s niet gevonden';
$string['artefactnotpublishable'] = 'Artefact %s is niet publiceerbaar in scherm %s';
$string['artefactnotrendered'] = 'Artefact niet verwerkt';
$string['at'] = 'bij';
$string['back'] = 'Terug';
$string['belongingto'] = 'Is van';
$string['bytes'] = 'bytes';
$string['cancel'] = 'Annuleer';
$string['cancelrequest'] = 'Annuleer aanvraag';
$string['cannotremovedefaultemail'] = 'Je kunt je primair e-mailadres niet verwijderen';
$string['cantchangepassword'] = 'Het is niet mogelijk je wachtwoord te wijzigen via deze interface - gebruik de interface van je instituut';
$string['captchadescription'] = 'Geef de tekens die je ziet op de afbeelding. Letters zijn niet hoofdlettergevoelig.';
$string['captchaimage'] = 'CAPTCHA afbeelding';
$string['captchaincorrect'] = 'Geef de letters zoals ze getoond worden op de afbeelding';
$string['captchatitle'] = 'CAPTCHA afbeelding';
$string['change'] = 'Wijzig';
$string['changepassword'] = 'Wijzig wachtwoord';
$string['changepasswordinfo'] = 'Je moet je wachtwoord wijzigen voor je verder kan gaan.';
$string['clambroken'] = 'Je beheerder heeft viruscontrole insgeschakeld voor geüploade bestanden maar heeft iets fout geconfigureerd.  Je bestand is niet geüpload. Je beheerder heeft een e-mail gekregen met de foutmelding, zodat de fout kan hersteld worden. Probeer later nog eens opnieuw.';
$string['clamdeletedfile'] = 'Het bestand is verwijderd';
$string['clamdeletedfilefailed'] = 'Het bestand kon niet verwijderd worden';
$string['clamemailsubject'] = '%s :: Clam AV melding';
$string['clamfailed'] = 'Clam AV werkt niet.  De foutboodschap is %s. Hier is de foutboodschap van Clam:';
$string['clamlost'] = 'Clam AV is geconfigureerd om bestanden te controleren tijdens het uploaden, maar het pad naar Clam AV, %s, is fout.';
$string['clammovedfile'] = 'Het bestand is in de quarantaine-map gezet.';
$string['clamunknownerror'] = 'Er is een onbekende fout opgetreden met Clam.';
$string['collapse'] = 'Inklappen';
$string['complete'] = 'Volledig';
$string['config'] = 'Configureer';
$string['confirminvitation'] = 'Bevestig uitnodiging';
$string['confirmpassword'] = 'Bevestig wachtwoord';
$string['contactus'] = 'Contacteer ons';
$string['cookiesnotenabled'] = 'Cookies zijn uitgeschakeld of geblokkeerd voor deze site in je browser. Voor Mahara moeten cookies ingeschakeld zijn om te kunnen aanmelden.';
$string['couldnotgethelp'] = 'Er is een fout opgetreden bij het ophalen van de helppagina';
$string['country.ad'] = 'Andorra';
$string['country.ae'] = 'Verenigde Arabische Emiraten';
$string['country.af'] = 'Afghanistan';
$string['country.ag'] = 'Antigua en Barbuda';
$string['country.ai'] = 'Anguilla';
$string['country.al'] = 'Albanië';
$string['country.am'] = 'Armenië';
$string['country.an'] = 'Nederlandse Antillen';
$string['country.ao'] = 'Angola';
$string['country.aq'] = 'Antarctica';
$string['country.ar'] = 'Argentinië';
$string['country.as'] = 'Amerikaans-Samoa';
$string['country.at'] = 'Oostenrijk';
$string['country.au'] = 'Australië';
$string['country.aw'] = 'Aruba';
$string['country.ax'] = 'Åland Eilanden';
$string['country.az'] = 'Azerbeidzjan';
$string['country.ba'] = 'Bosnië en Herzegovina';
$string['country.bb'] = 'Barbados';
$string['country.bd'] = 'Bangladesh';
$string['country.be'] = 'België';
$string['country.bf'] = 'Burkina Faso';
$string['country.bg'] = 'Bulgarije';
$string['country.bh'] = 'Bahrein';
$string['country.bi'] = 'Burundi';
$string['country.bj'] = 'Benin';
$string['country.bm'] = 'Bermuda';
$string['country.bn'] = 'Brunei Darussalam';
$string['country.bo'] = 'Bolivia';
$string['country.br'] = 'Brazilië';
$string['country.bs'] = 'Bahamas';
$string['country.bt'] = 'Bhutan';
$string['country.bv'] = 'Bouvet';
$string['country.bw'] = 'Botswana';
$string['country.by'] = 'Wit-Rusland';
$string['country.bz'] = 'Belize';
$string['country.ca'] = 'Canada';
$string['country.cc'] = 'Cocos (Keeling) Islands';
$string['country.cd'] = 'Congo, Democratische Republiek';
$string['country.cf'] = 'Centraal-Afrikaanse Republiek';
$string['country.cg'] = 'Congo';
$string['country.ch'] = 'Zwitserland';
$string['country.ci'] = 'Ivoorkust';
$string['country.ck'] = 'Cookeilanden';
$string['country.cl'] = 'Chili';
$string['country.cm'] = 'Kameroen';
$string['country.cn'] = 'China';
$string['country.co'] = 'Colombia';
$string['country.cr'] = 'Costa Rica';
$string['country.cs'] = 'Servië en Montenegro';
$string['country.cu'] = 'Cuba';
$string['country.cv'] = 'Kaapverdië';
$string['country.cx'] = 'Christmas Island';
$string['country.cy'] = 'Cyprus';
$string['country.cz'] = 'Tsjechië';
$string['country.de'] = 'Duitsland';
$string['country.dj'] = 'Djibouti';
$string['country.dk'] = 'Denemarken';
$string['country.dm'] = 'Dominica';
$string['country.do'] = 'Dominicaanse Republiek';
$string['country.dz'] = 'Algerije';
$string['country.ec'] = 'Ecuador';
$string['country.ee'] = 'Estland';
$string['country.eg'] = 'Egypte';
$string['country.eh'] = 'Westelijke Sahara';
$string['country.er'] = 'Eritrea';
$string['country.es'] = 'Spanje';
$string['country.et'] = 'Ethiopië';
$string['country.fi'] = 'Finland';
$string['country.fj'] = 'Fiji';
$string['country.fk'] = 'Falklandeilanden (Malvinas)';
$string['country.fm'] = 'Micronesia, Federale Staten van';
$string['country.fo'] = 'Faeröer';
$string['country.fr'] = 'Frankrijk';
$string['country.ga'] = 'Gabon';
$string['country.gb'] = 'Verenigd Koninkrijk';
$string['country.gd'] = 'Grenada';
$string['country.ge'] = 'Georgië';
$string['country.gf'] = 'Frans-Guyana';
$string['country.gg'] = 'Guernsey';
$string['country.gh'] = 'Ghana';
$string['country.gi'] = 'Gibraltar';
$string['country.gl'] = 'Groenland';
$string['country.gm'] = 'Gambia';
$string['country.gn'] = 'Guinee';
$string['country.gp'] = 'Guadeloupe';
$string['country.gq'] = 'Equatoriaal-Guinea';
$string['country.gr'] = 'Griekenland';
$string['country.gs'] = 'Zuid-Georgië en de Zuidelijke Sandwicheilanden';
$string['country.gt'] = 'Guatemala';
$string['country.gu'] = 'Guam';
$string['country.gw'] = 'Guinee-Bissau';
$string['country.gy'] = 'Guyana';
$string['country.hk'] = 'Hongkong';
$string['country.hm'] = 'Heardeiland en McDonaldeilanden';
$string['country.hn'] = 'Honduras';
$string['country.hr'] = 'Kroatië';
$string['country.ht'] = 'Haïti';
$string['country.hu'] = 'Hongarije';
$string['country.id'] = 'Indonesië';
$string['country.ie'] = 'Ierland';
$string['country.il'] = 'Israël';
$string['country.im'] = 'Man, Eiland';
$string['country.in'] = 'India';
$string['country.io'] = 'Brits Territorium in de Indische Oceaan';
$string['country.iq'] = 'Irak';
$string['country.ir'] = 'Iran, Islamitische Republiek';
$string['country.is'] = 'Ijsland';
$string['country.it'] = 'Italië';
$string['country.je'] = 'Jersey';
$string['country.jm'] = 'Jamaica';
$string['country.jo'] = 'Jordanië';
$string['country.jp'] = 'Japan';
$string['country.ke'] = 'Kenia';
$string['country.kg'] = 'Kirgizië';
$string['country.kh'] = 'Cambodja';
$string['country.ki'] = 'Kiribati';
$string['country.km'] = 'Comoren';
$string['country.kn'] = 'Saint Kitts and Nevis';
$string['country.kp'] = 'Korea, Democratische Volksrepubliek';
$string['country.kr'] = 'Korea, Republiek';
$string['country.kw'] = 'Koeweit';
$string['country.ky'] = 'Kaaimaneilanden';
$string['country.kz'] = 'Kazachstan';
$string['country.la'] = 'Laos, Democratische Volksrepubliek';
$string['country.lb'] = 'Libanon';
$string['country.lc'] = 'Saint Lucia';
$string['country.li'] = 'Liechtenstein';
$string['country.lk'] = 'Sri Lanka';
$string['country.lr'] = 'Liberia';
$string['country.ls'] = 'Lesotho';
$string['country.lt'] = 'Litouwen';
$string['country.lu'] = 'Luxemburg, Groothertogdom';
$string['country.lv'] = 'Letland';
$string['country.ly'] = 'Libië (Grote Libisch-Arabische Socialistische Volks-Jamahiriyah)';
$string['country.ma'] = 'Marokko';
$string['country.mc'] = 'Monaco';
$string['country.md'] = 'Moldavië, Republiek';
$string['country.mg'] = 'Madagascar';
$string['country.mh'] = 'Marshalleilanden';
$string['country.mk'] = 'Macedonië, Voormalige Joegoslavische Republiek';
$string['country.ml'] = 'Mali';
$string['country.mm'] = 'Myanmar';
$string['country.mn'] = 'Mongolië';
$string['country.mo'] = 'Macau';
$string['country.mp'] = 'Noordelijke Marianen';
$string['country.mq'] = 'Martinique';
$string['country.mr'] = 'Mauritanië';
$string['country.ms'] = 'Montserrat';
$string['country.mt'] = 'Malta';
$string['country.mu'] = 'Mauritius';
$string['country.mv'] = 'Maldiven';
$string['country.mw'] = 'Malawi';
$string['country.mx'] = 'Mexico';
$string['country.my'] = 'Maleisië';
$string['country.mz'] = 'Mozambique';
$string['country.na'] = 'Namibië';
$string['country.nc'] = 'Nieuw-Caledonië';
$string['country.ne'] = 'Niger';
$string['country.nf'] = 'Norfolk';
$string['country.ng'] = 'Nigeria';
$string['country.ni'] = 'Nicaragua';
$string['country.nl'] = 'Nederland';
$string['country.no'] = 'Noorwegen';
$string['country.np'] = 'Nepal';
$string['country.nr'] = 'Nauru';
$string['country.nu'] = 'Niue';
$string['country.nz'] = 'Nieuw-Zeeland';
$string['country.om'] = 'Oman';
$string['country.pa'] = 'Panama';
$string['country.pe'] = 'Peru';
$string['country.pf'] = 'Frans-Polynesië';
$string['country.pg'] = 'Papoea-Nieuw-Guinea';
$string['country.ph'] = 'Filipijnen';
$string['country.pk'] = 'Pakistan';
$string['country.pl'] = 'Polen';
$string['country.pm'] = 'Saint-Pierre en Miquelon';
$string['country.pn'] = 'Pitcairneilanden';
$string['country.pr'] = 'Puerto Rico';
$string['country.ps'] = 'Palestijns Gebied, Bezet';
$string['country.pt'] = 'Portugal';
$string['country.pw'] = 'Palau';
$string['country.py'] = 'Paraguay';
$string['country.qa'] = 'Qatar';
$string['country.re'] = 'Réunion';
$string['country.ro'] = 'Roemenië';
$string['country.ru'] = 'Russische Federatie';
$string['country.rw'] = 'Rwanda';
$string['country.sa'] = 'Saoedi-Arabië';
$string['country.sb'] = 'Salomonseilanden';
$string['country.sc'] = 'Seychellen';
$string['country.sd'] = 'Soedan';
$string['country.se'] = 'Zweden';
$string['country.sg'] = 'Singapore';
$string['country.sh'] = 'Sint-Helena';
$string['country.si'] = 'Slovenië';
$string['country.sj'] = 'Svalbard en Jan Mayen';
$string['country.sk'] = 'Slowakije';
$string['country.sl'] = 'Sierra Leone';
$string['country.sm'] = 'San Marino';
$string['country.sn'] = 'Senegal';
$string['country.so'] = 'Somalië';
$string['country.sr'] = 'Suriname';
$string['country.st'] = 'Sao Tomé en Principe';
$string['country.sv'] = 'El Salvador';
$string['country.sy'] = 'Syrische Arabische Republiek';
$string['country.sz'] = 'Swaziland';
$string['country.tc'] = 'Turks- en Caicoseilanden';
$string['country.td'] = 'Tsjaad';
$string['country.tf'] = 'Franse Zuidelijke en Antarctische Gebieden';
$string['country.tg'] = 'Togo';
$string['country.th'] = 'Thailand';
$string['country.tj'] = 'Tadzjikistan';
$string['country.tk'] = 'Tokelau';
$string['country.tl'] = 'Timor-Leste';
$string['country.tm'] = 'Turkmenistan';
$string['country.tn'] = 'Tunesië';
$string['country.to'] = 'Tonga';
$string['country.tr'] = 'Turkije';
$string['country.tt'] = 'Trinidad en Tobago';
$string['country.tv'] = 'Tuvalu';
$string['country.tw'] = 'Taiwan';
$string['country.tz'] = 'Tanzania, Verenigde Republiek';
$string['country.ua'] = 'Oekraïne';
$string['country.ug'] = 'Uganda';
$string['country.um'] = 'Kleine afgelegen eilanden van de Verenigde Staten';
$string['country.us'] = 'Verenigde Staten';
$string['country.uy'] = 'Uruguay';
$string['country.uz'] = 'Oezbekistan';
$string['country.va'] = 'Vaticaanstad';
$string['country.vc'] = 'Saint Vincent en de Grenadines';
$string['country.ve'] = 'Venezuela';
$string['country.vg'] = 'Maagdeneilanden, Britse';
$string['country.vi'] = 'Maagdeneilanden, Amerikaanse';
$string['country.vn'] = 'Vietnam';
$string['country.vu'] = 'Vanuatu';
$string['country.wf'] = 'Wallis en Futuna';
$string['country.ws'] = 'Samoa';
$string['country.ye'] = 'Yemen';
$string['country.yt'] = 'Mayotte';
$string['country.za'] = 'Zuid-Afrika';
$string['country.zm'] = 'Zambia';
$string['country.zw'] = 'Zimbabwe';
$string['day'] = 'dag';
$string['days'] = 'dagen';
$string['debugemail'] = 'OPMERKING: Deze e-mail is bedoeld voor %s <%s> maar is naar jou gestuurd volgens de "sendallemailto" configuratieinstelling.';
$string['decline'] = 'Afwijzen';
$string['default'] = 'Standaard';
$string['delete'] = 'Verwijder';
$string['description'] = 'Beschrijving';
$string['displayname'] = 'Getoonde naam';
$string['done'] = 'Klaar';
$string['edit'] = 'Bewerk';
$string['editing'] = 'Bewerken';
$string['editmyprofilepage'] = 'Bewerk profielpagina';
$string['email'] = 'E-mail';
$string['emailaddress'] = 'E-mailadres';
$string['emailaddressorusername'] = 'E-mailadres of gebruikersnaam';
$string['emailname'] = 'Mahara Systeem';
$string['emailnotsent'] = 'Sturen van e-mail mislukt. Foutbericht: "%s"';
$string['emailtoolong'] = 'E-mailadressen kunnen niet langer zijn dan 255 tekens';
$string['errorprocessingform'] = 'Er is een fout opgetreden bij het insturen van dit formulier. Controleer de gemarkeerde velden en probeer opnieuw.';
$string['expand'] = 'Uitklappen';
$string['filenotimage'] = 'Het bestand dat je uploade is geen geldige afbeelding. Het moet een PNG, JPEG of GIF bestand zijn.';
$string['fileunknowntype'] = 'Het type van je geüploade bestand kon niet bepaald worden.Je bestand kan beschadigd zijn, maar het kan ook om een configuratiefout gaan. Contacteer je beheerder.';
$string['filter'] = 'Filter';
$string['findfriends'] = 'Zoek vrienden';
$string['findgroups'] = 'Zoek groepen';
$string['first'] = 'Eerste';
$string['firstname'] = 'Voornaam';
$string['firstpage'] = 'Eerste pagina';
$string['forgotpassemailsendunsuccessful'] = 'De e-mail kon blijkbaar niet verzonden worden. Dit is onze fout. Probeer later nog eens opnieuw';
$string['forgotpassnosuchemailaddressorusername'] = 'Het e-mailadres of de gebruikersnaam die je ingegeven hebt, komt niet voor in onze databank';
$string['forgotpasswordenternew'] = 'Geef je nieuw wachtwoord in om verder te gaan';
$string['forgotusernamepassword'] = 'Gebruikersnaam of wachtwoord vergeten?';
$string['forgotusernamepasswordemailmessagehtml'] = '<p>Beste %s,</p>

<p>Je gebruikersnaam/wachtwoord voor je account op %s werd aangevraagd.</p>

<p>Je gebruikersnaam is <strong>%s</strong>.</p>

<p>Als je je wachtwoord wil wijzigen, volg dan onderstaande link:</p>

<p><a href="%s">%s</a></p>

<p>Als je geen wijziging van je wachtwoord hebt aangevraagd, negeer dan deze e-mail.</p>

<p>Als je hierbij nog vragen hebt, <a href="%s">contacteer ons dan</a>.</p>

<p>Vriendelijke groet, %s Site-beheerder</p>';
$string['forgotusernamepasswordemailmessagetext'] = 'Beste %s,

Je gebruikersnaam/wachtwoord voor je account op %s werd aangevraagd.

Je gebruikersnaam is %s.

Als je je wachtwoord wil wijzigen, volg dan onderstaande link:

%s

Als je geen wijziging van je wachtwoord hebt aangevraagd, negeer dan deze e-mail.

Als je hierbij nog vragen hebt, contacteer ons dan:

%s

Vriendelijke groet, %s Site-beheerder';
$string['forgotusernamepasswordemailsubject'] = 'Gebruikersnaam/wachtwoord voor %s';
$string['forgotusernamepasswordtext'] = '<p>Als je je gebruikersnaam of wachtwoord vergeten bent, geef je e-mailadres van je profiel en we zullen je een bericht sturen waarmee je jezelf een nieuw wachtwoord kan geven.</p>
<p>Als je je gebruikersnaam nog weet en je bent alleen je wachtwoord vergeten, dan kun je ook je gebruikersnaam geven.</p>';
$string['formatpostbbcode'] = 'Je kunt je bericht opmaken door BBCode te gebruiken. %sMeer weten%s';
$string['fullname'] = 'Volledige naam';
$string['go'] = 'Ga';
$string['groups'] = 'Groepen';
$string['height'] = 'Hoogte';
$string['heightshort'] = 'h';
$string['home'] = 'Home';
$string['image'] = 'Afbeelding';
$string['importedfrom'] = 'Geïmporteerd van %s';
$string['incomingfolderdesc'] = 'Bestanden geïmporteerd van andere genetwerkte hosts';
$string['installedplugins'] = 'Geïnstalleerde plugins';
$string['institution'] = 'Instituut';
$string['institutionexpirywarning'] = 'Waarschuwing: instituutslidmaatschap verloopt';
$string['institutionexpirywarninghtml'] = '<p>Beste %s,</p>
    
<p>Je lidmaatschap voor %s op %s zal verlopen op %s.</p>

<p>Als je je lidmaatschap wil verlengen of je hebt vragen hierover, : <a href="%s">contacteer ons dan</a>.</p>

<p>Vriendelijke groet, %s Site-beheerder</p>';
$string['institutionexpirywarningtext'] = 'Beste %s,

Je lidmaatschap voor %s op %s zal verlopen op %s.

Als je je lidmaatschap wil verlengen of je hebt vragen hierover, contacteer ons dan:

%s

Vriendelijke groet, %s Site-beheerder';
$string['institutionfull'] = 'Het instituut dat je gekozen hebt, aanvaardt geen registraties meer.';
$string['institutionmemberconfirmmessage'] = 'Je bent toegevoegd als lid van %s.';
$string['institutionmemberconfirmsubject'] = 'Bevestiging instituutslidmaatschap';
$string['institutionmemberrejectmessage'] = 'Je aanvraag voor lidmaatschap van %s is afgewezen.';
$string['institutionmemberrejectsubject'] = 'Instituutslidmaatschap afgewezen';
$string['institutionmembership'] = 'Instituutslidmaatschap';
$string['institutionmembershipdescription'] = 'De instituten waarvan je lid bent kun je in deze lijst zien.  Je kunt ook het lidmaatschap van een instituut aanvragen. Als een instituut je uitgenodigd heeft, dan kan je hier de uitnodiging accepteren of afwijzenen.';
$string['invalidsesskey'] = 'Ongeldige sessiesleutel';
$string['invitedgroup'] = 'groepsuitnodiging';
$string['invitedgroups'] = 'groepsuitnodigingen';
$string['javascriptnotenabled'] = 'Javascript is voor deze site uitgeschakeld in je browser. Voor Mahara moet javascript ingeschakeld zijn om te kunnen aanmelden.';
$string['joininstitution'] = 'Word lid van instituut';
$string['language'] = 'Taal';
$string['last'] = 'Laatste';
$string['lastminutes'] = 'Afgelopen %s minuten';
$string['lastname'] = 'Achternaam';
$string['lastpage'] = 'Laatste pagina';
$string['leaveinstitution'] = 'Verlaat instituut';
$string['linksandresources'] = 'Links en Bronnen';
$string['loading'] = 'Laden ...';
$string['loggedinusersonly'] = 'Alleen ingelogde gebruikers';
$string['loggedoutok'] = 'Afmelden gelukt';
$string['login'] = 'Log in';
$string['loginfailed'] = 'Je gebruikersnaam en/of wachtwoord zijn fout.';
$string['loginto'] = 'Log in bij %s';
$string['logout'] = 'Afmelden';
$string['lostusernamepassword'] = 'Gebruikersnaam/wachtwoord verloren';
$string['membershipexpiry'] = 'Lidmaatschap verloopt';
$string['message'] = 'Bericht';
$string['messagesent'] = 'Je bericht is verzonden';
$string['months'] = 'maanden';
$string['more...'] = 'Meer ...';
$string['mustspecifyoldpassword'] = 'Je moet je huidig wachtwoord opgeven';
$string['myfriends'] = 'Mijn vrienden';
$string['mygroups'] = 'Mijn groepen';
$string['myportfolio'] = 'Mijn Portfolio';
$string['myviews'] = 'Mijn pagina\'s';
$string['name'] = 'Naam';
$string['namedfieldempty'] = 'Het vereiste veld "%s" is leeg';
$string['newpassword'] = 'Nieuw wachtwoord';
$string['next'] = 'Volgende';
$string['nextpage'] = 'Volgende pagina';
$string['no'] = 'Nee';
$string['nodeletepermission'] = 'Je hebt het recht niet om dit artefact te verwijderen';
$string['noeditpermission'] = 'Je hebt het recht niet om dit artefact te bewerken';
$string['noenddate'] = 'geen einddatum';
$string['nohelpfound'] = 'Geen hulp gevonden voor dit item';
$string['nohelpfoundpage'] = 'Geen hulp gevonden voor deze pagina';
$string['noinstitutionsetpassemailmessagehtml'] = '<p>Beste %s,</p>

<p>Je bent niet langer lid van %s.</p>
<p>Je kunt %s blijven gebruiken met je huidige gebruikersnaam %s, maar je moet een nieuw wachtwoord instellen voor je account.</p>

<p>Volg onderstaande link om verder te gaan met dit proces.</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>

<p>Als je hierbij vragen hebt, aarzel dan niet ons te contacteren <a href="%scontact.php">ons te contacteren</a>.</p>

<p>Vriendelijke groet, %s Site-beheerder</p>

<p><a href="%sforgotpass.php?key=%s">%sforgotpass.php?key=%s</a></p>';
$string['noinstitutionsetpassemailmessagetext'] = 'Beste %s,

Je bent niet langer lid van %s.
Je kunt %s blijven gebruiken met je huidige gebruikersnaam %s, maar je moet een nieuw wachtwoord instellen voor je account.

Volg onderstaande link om verder te gaan met dit proces.

%sforgotpass.php?key=%s

Als je hierbij vragen hebt, aarzel dan niet ons te contacteren.

%scontact.php

Vriendelijke groet, %s Site-beheerder

%sforgotpass.php?key=%s';
$string['noinstitutionsetpassemailsubject'] = '%s: Lidmaatschap van %s';
$string['none'] = 'Geen';
$string['noresultsfound'] = 'Niets gevonden';
$string['nosendernamefound'] = 'Geen afzendernaam gevonden';
$string['nosessionreload'] = 'Pagina herladen om in te loggen';
$string['nosuchpasswordrequest'] = 'Er is zo geen wachtwoordaanvraag';
$string['notifications'] = 'Meldingen';
$string['notinstallable'] = 'Niet installeerbaar!';
$string['notinstalledplugins'] = 'Niet geïnstalleerde plugins';
$string['notphpuploadedfile'] = 'Het bestand is verloren gegaan tijdens het uploaden. Dit zou niet mogen gebeuren. Contacteer je systeembeheerder voor meer informatie.';
$string['oldpassword'] = 'Huidig wachtwoord';
$string['onlineusers'] = 'Online gebruikers';
$string['optionalinstitutionid'] = 'Instituut ID (optioneel)';
$string['password'] = 'Wachtwoord';
$string['passwordchangedok'] = 'Je wachtwoord is gewijzigd';
$string['passwordhelp'] = 'Het wachtwoord dat je gebruikt om toegang te krijgen tot dit systeem';
$string['passwordnotchanged'] = 'Je hebt je wachtwoord niet gewijzigd, kies een nieuw';
$string['passwordsaved'] = 'Je nieuw wachtwoord is bewaard';
$string['passwordsdonotmatch'] = 'De wachtwoorden komen niet overeen';
$string['passwordtooeasy'] = 'Je wachtwoord is niet veilig. Kies een meer complex wachtwoord';
$string['pendingfriend'] = 'vriendschapsverzoek';
$string['pendingfriends'] = 'vriendschapsverzoeken';
$string['phpuploaderror'] = 'Er is een fout opgetreden tijdens de bestandsupload: %s (Foutcode %s)';
$string['phpuploaderror_1'] = 'Het geüploade bestand is groter dan de upload_max_filesize waarde in php.ini.';
$string['phpuploaderror_2'] = 'Het geüploade bestand is groter dan de MAX_FILE_SIZE instelling die opgegeven is in het HTML formulier.';
$string['phpuploaderror_3'] = 'Het bestand is slechts gedeeltelijk geüpload.';
$string['phpuploaderror_4'] = 'Er is geen bestand geüpload.';
$string['phpuploaderror_6'] = 'Tijdelijke map ontbreekt.';
$string['phpuploaderror_7'] = 'Bestand naar schijf schrijven mislukt.';
$string['phpuploaderror_8'] = 'De upload van het bestand is gestopt door een extentie.';
$string['pleasedonotreplytothismessage'] = 'Dit bericht niet beantwoorden.';
$string['plugintype'] = 'Plugin type';
$string['preferences'] = 'Voorkeuren';
$string['preferredname'] = 'Voorkeursnaam';
$string['previous'] = 'Vorige';
$string['prevpage'] = 'Vorige pagina';
$string['primaryemailinvalid'] = 'Je primair e-mailadres is niet geldig';
$string['privacystatement'] = 'Privacy verklaring';
$string['processing'] = 'Verwerken';
$string['profile'] = 'profiel';
$string['profileimage'] = 'Profielafbeelding';
$string['pwchangerequestsent'] = 'Je zou binnen enkele ogenblikken een e-mail moeten ontvangen met daarin een link die je kunt gebruiken om het wachtwoord van je account te wijzigen';
$string['quarantinedirname'] = 'quarantaine';
$string['query'] = 'Query';
$string['querydescription'] = 'De te zoeken woorden';
$string['quota'] = 'Quota';
$string['quotausage'] = 'U heeft <span id="quota_used">%s</span> verbruikt van uw <span id="quota_total">%s</span> quota.';
$string['reallyleaveinstitution'] = 'Ben je er zeker van dat je dit instituut wil verlaten?';
$string['reason'] = 'Reden';
$string['register'] = 'Registreer';
$string['registeringdisallowed'] = 'Je kunt je nu niet registreren voor dit systeem';
$string['registerstep1description'] = 'Welcom! Om deze site te kunnen gebruiken moet je je eerst registreren. Je moet ook akkoord gaan met de  <a href="terms.php">gebruiksvoorwaarden</a>. De gegevens die we hier verzamelen worden bewaard volgens onze <a href="privacy.php">privacy verklaring</a>.';
$string['registerstep3fieldsmandatory'] = '<h3>Je moet de verplichte profielvelden invullen</h3><p>Onderstaande velden zijn vereist. Je registratie is niet volledig als ze niet ingevuld zijn.</p>';
$string['registerstep3fieldsoptional'] = '<h3>Kies (optioneel) een profielafbeelding</h3><p>Je bent nu geregistreerd bij %s! Je kan nu (optioneel) een profielafbeelding kiezen die als jouw avatar getoond zal worden.</p>';
$string['registrationcomplete'] = 'Bedankt voor je registratie bij %s';
$string['registrationnotallowed'] = 'Het instituut dat je gekozen hebt, aanvaardt geen zelfregistratie.';
$string['reject'] = 'Niet aanvaarden';
$string['remotehost'] = 'Genetwerkte host %s';
$string['remove'] = 'Verwijder';
$string['republish'] = 'Publiceer';
$string['requestmembershipofaninstitution'] = 'Lidmaatschap van een instituut aanvragen';
$string['requiredfieldempty'] = 'Een vereist veld is leeg';
$string['result'] = 'resultaat';
$string['results'] = 'resultaten';
$string['returntosite'] = 'Keer terug naar de site';
$string['save'] = 'Bewaar';
$string['search'] = 'Zoeken';
$string['searchusers'] = 'Zoek gebruikers';
$string['select'] = 'Selecteer';
$string['selfsearch'] = 'Doorzoek mijn portfolio';
$string['send'] = 'Stuur';
$string['sendmessage'] = 'Stuur bericht';
$string['sendrequest'] = 'Stuur verzoek';
$string['sessiontimedout'] = 'Je sessie is verlopen, geef je gebruikersnaam en wachtwoord om aan te melden';
$string['sessiontimedoutpublic'] = 'Je sessie is verlopen. Ga naar <a href="%s">log in</a> om verder te werken';
$string['sessiontimedoutreload'] = 'Je sessie is verlopen. Herlaad de pagina om terug in te loggen';
$string['settings'] = 'Instellingen';
$string['settingssaved'] = 'Instellingen bewaard';
$string['settingssavefailed'] = 'Instellingen bewaren mislukt';
$string['showtags'] = 'Toon mijn tags';
$string['siteadministration'] = 'Site-beheer';
$string['siteclosed'] = 'De site is tijdelijk gesloten voor een databank-upgrade. Site-beheerders kunnen aanmelden.';
$string['siteclosedlogindisabled'] = 'De site is tijdelijk gesloten voor een databank-upgrade.  <a href="%s">Upgrade nu uitvoeren.</a>';
$string['sitecontentnotfound'] = '%s tekst niet beschikbaar';
$string['sizeb'] = 'b';
$string['sizegb'] = 'GB';
$string['sizekb'] = 'KB';
$string['sizemb'] = 'MB';
$string['strftimenotspecified'] = 'Niet gespecificeerd';
$string['studentid'] = 'ID-nummer';
$string['subject'] = 'Onderwerp';
$string['submit'] = 'Insturen';
$string['system'] = 'Systeem';
$string['tags'] = 'Tags';
$string['tagsdesc'] = 'Geef tags voor dit item, gescheiden door een komma.';
$string['tagsdescprofile'] = 'Geef tags voor dit item, gescheiden door een komma. Items met als tag \'profiel\' worden in je zijbalk getoond.';
$string['termsandconditions'] = 'Voorwaarden';
$string['thisistheprofilepagefor'] = 'Dit is de profielpagina van %s';
$string['unknownerror'] = 'Onbekende fout (0x20f91a0)';
$string['unreadmessage'] = 'ongelezen bericht';
$string['unreadmessages'] = 'ongelezen berichten';
$string['update'] = 'Updaten';
$string['updatefailed'] = 'Bijwerking gefaald';
$string['upload'] = 'Upload';
$string['uploadedfiletoobig'] = 'Het bestand was te groot. Vraag je site-beheerder voor meer informatie.';
$string['useradministration'] = 'Gebruikersbeheer';
$string['username'] = 'Gebruikersnaam';
$string['usernamehelp'] = 'De gebruikersnaam die je gekregen hebt om toegang te krijgen tot dit systeem.';
$string['users'] = 'Gebruikers';
$string['view'] = 'Pagina';
$string['viewmyprofilepage'] = 'Bekijk profielpagina';
$string['views'] = 'Pagina\'s';
$string['virusfounduser'] = 'Het bestand dat je upgeload hebt, %s, is op virussen gescand en blijkt geïnfecteerd te zijn! Je bestand is niet geüpload.';
$string['virusrepeatmessage'] = 'Gebruiker %s heeft meerdere met virussen geïnfecteerde bestanden geüpload.';
$string['virusrepeatsubject'] = 'Waarschuwing: %s upload herhaaldelijk virussen.';
$string['weeks'] = 'weken';
$string['width'] = 'Breedte';
$string['widthshort'] = 'b';
$string['years'] = 'jaren';
$string['yes'] = 'Ja';
$string['youareamemberof'] = 'Je bent lid van %s';
$string['youaremasqueradingas'] = 'Je bent ingelogd als %s.';
$string['youhavebeeninvitedtojoin'] = 'Je bent uitgenodigd door %s';
$string['youhavenottaggedanythingyet'] = 'Je hebt nog geen tags gegeven';
$string['youhaverequestedmembershipof'] = 'Je hebt het lidmaatschap aangevraagd voor %s';
$string['youraccounthasbeensuspended'] = 'Je account is uitgeschakeld';
$string['youraccounthasbeensuspendedreasontext'] = 'Je account bij %s is uitgeschakeld door %s. Reden:

%s';
$string['youraccounthasbeensuspendedtext2'] = 'Je account bij %s is uitgeschakeld door %s.';
$string['youraccounthasbeenunsuspended'] = 'Je account is terug ingeschakeld';
$string['youraccounthasbeenunsuspendedtext2'] = 'Je account bij %s is terug ingeschakeld. Je kunt opnieuw inloggen en de site gebruiken.';
$string['yournewpassword'] = 'Je nieuwe wachtwoord';
$string['yournewpasswordagain'] = 'Herhaal je nieuwe wachtwoord';
?>
