<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage auth-internal
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['internal'] = 'Intern';
$string['title'] = 'Intern';
$string['description'] = 'Communiceer met Mahara\'s database';

$string['completeregistration'] = 'Voltooi Registratie';
$string['emailalreadytaken'] = 'Dit e-mailadres is hier al geregistreerd';
$string['iagreetothetermsandconditions'] = 'Ik ga akkoord met de gebruiksvoorwaarden';
$string['passwordformdescription'] = 'Je wachtwoord moet minimaal zes tekens lang zijn en tenminste een cijfer en twee letters bevatten';
$string['passwordinvalidform'] = 'Je wachtwoord moet minimaal zes tekens lang zijn en tenminste een cijfer en twee letters bevatten';
$string['registeredemailsubject'] = 'Je bent geregistreerd op %s';
$string['registeredemailmessagetext'] = 'Hallo %s,

Dankje voor het registreren van je account op %s. Volg alstublieft deze link om het registratieproces te voltooien:

' . get_config('wwwroot') . 'register.php?key=%s

--
Met vriendelijke groet,
Het %s Team';
$string['registeredemailmessagehtml'] = '<p>Hallo %s,</p>
<p>Dankje voor het registreren van je account op %s. Volg alstublieft deze link om het registratieproces te voltooien:</p>
<p><a href="' . get_config('wwwroot') . 'register.php?key=%s">'
. get_config('wwwroot') . 'register.php?key=%s</a></p>
<pre>--
Met vriendelijke groet,
Het %s Team</pre>';
$string['registeredok'] = '<p>Je registratie is gelukt. Controleer alstublieft je e-mail account voor verdere instructies over hoe je je account kunt activeren</p>';
$string['registrationnosuchkey'] = 'Excuses, er is geen registratie die overeen komt met deze sleutel. Mogelijk heb je langer dan 24 uur gewacht met het voltooien van je registratie? Anders is het mogelijk onze fout.';
$string['registrationunsuccessful'] = 'Excuses, je poging te registreren is mislukt. Dit is een fout van onze kant. Probeer het later alstublieft nog eens.';
$string['usernamealreadytaken'] = 'Excuses, deze gebruikersnaam is al bezet';
$string['usernameinvalidform'] = 'Je gebruikersnaam mag alleen alfanumerieke karakters, punten, underscores en @ symbolen bevatten. Verder moet die tussen 3 en 30 tekens lang zijn.';
$string['youmaynotregisterwithouttandc'] = 'Je mag zich niet registreren tenzij je instemt met de <a href="terms.php">Gebruiksvoorwaarden</a>';
$string['youmustagreetothetermsandconditions'] = 'Je moet akkoord gaan met de <a href="terms.php">Gebruiksvoorwaarden</a>';

?>
