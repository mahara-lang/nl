<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage blocktype-externalfeeds
 * @author     Catalyst IT Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Externe feed';
$string['description'] = 'Inbedden van een externe RSS- of ATOM-feed';
$string['feedlocation'] = 'Feed-locatie';
$string['feedlocationdesc'] = 'Geldige URL van een RSS- of ATOM-feed';
$string['showfeeditemsinfull'] = 'Feed-items volledig weergeven?';
$string['showfeeditemsinfulldesc'] = 'Korte inhoud weergeven van feed-items of ook voor elk de beschrijving';
$string['invalidurl'] = 'Deze URL is ongeldig. Je kunt alleen feeds weergeven voor http en https URL\'s.';
$string['invalidfeed'] = 'Deze feed blijkt ongeldig. Dit is de foutmelding: %s';
$string['lastupdatedon'] = 'Laatst bijgewerkt op %s';
$string['defaulttitledescription'] = 'Als je dit veld leeg laat, wordt de titel van de feed gebruikt';
?>
