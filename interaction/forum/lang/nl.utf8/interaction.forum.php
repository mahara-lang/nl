<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/nl.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2008 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['Body'] = 'Berichttekst';
$string['Close'] = 'Sluiten';
$string['Closed'] = 'Gesloten';
$string['Count'] = 'Telling';
$string['Key'] = 'Sleutel';
$string['Moderators'] = 'Moderators';
$string['Open'] = 'Open';
$string['Order'] = 'Volgorde';
$string['Post'] = 'Bericht';
$string['Poster'] = 'Afzender';
$string['Posts'] = 'Berichten';
$string['Reply'] = 'Reageer';
$string['Sticky'] = 'Sticky';
$string['Subject'] = 'Onderwerp';
$string['Subscribe'] = 'Abonneren';
$string['Subscribed'] = 'Geabonneerd';
$string['Topic'] = 'Discussie';
$string['Topics'] = 'Discussies';
$string['Unsticky'] = 'Unsticky';
$string['Unsubscribe'] = 'Uitschrijven';
$string['addpostsuccess'] = 'Aanmaken bericht gelukt';
$string['addtitle'] = 'Toevoegen forum';
$string['addtopic'] = 'Toevoegen topic';
$string['addtopicsuccess'] = 'Toevoegen topic gelukt';
$string['autosubscribeusers'] = 'Gebruikers automatisch inschrijven voor meldingen van dit forum?';
$string['autosubscribeusersdescription'] = 'Kies of gebruikers van groepen automatisch meldingen van dit forum krijgen';
$string['cantaddposttoforum'] = 'Je bent niet bevoegd berichten te sturen naar dit forum';
$string['cantaddposttotopic'] = 'Je bent niet bevoegd berichten te plaatsen binnen dit topic';
$string['cantaddtopic'] = 'Je bent niet bevoegd discussies toe te voegen aan dit forum';
$string['cantdeletepost'] = 'Je bent niet bevoegd berichten te verwijderen van dit forum';
$string['cantdeletethispost'] = 'Je bent niet bevoegd dit bericht te verwijderen';
$string['cantdeletetopic'] = 'Je bent niet bevoegd topics van dit forum te verwijderen';
$string['canteditpost'] = 'Je bent niet bevoegd dit bericht te bewerken';
$string['cantedittopic'] = 'Je bent niet bevoegd om deze discussie te bewerken';
$string['cantfindforum'] = 'Het forum met id %s kon niet worden gevonden';
$string['cantfindpost'] = 'Het bericht met id %s kon niet worden gevonden';
$string['cantfindtopic'] = 'De discussie met id %s kon niet worden gevonden';
$string['cantviewforums'] = 'Je bent niet bevoegd forums te bekijken binnen deze groep';
$string['cantviewtopic'] = 'Je bent niet bevoegd discussies van dit forum te bekijken';
$string['chooseanaction'] = 'Kies een actie';
$string['clicksetsubject'] = 'Klik om een discussie te starten';
$string['closeddescription'] = 'Op gesloten topics kan alleen gereageerd worden door de moderator en groepseigenaar';
$string['currentmoderators'] = 'Huidige moderator';
$string['deletedpost'] = 'Dit bericht is verwijderd';
$string['deleteforum'] = 'Verwijder forum';
$string['deletepost'] = 'Verwijder bericht';
$string['deletepostsuccess'] = 'Bericht verwijderen gelukt';
$string['deletepostsure'] = 'Bent je er zeker van dat je dit wil doen? Het kan niet ongedaan gemaakt worden.';
$string['deletetopic'] = 'Verwijder discussie';
$string['deletetopicsuccess'] = 'Discussie verwijderen gelukt';
$string['deletetopicsure'] = 'Bent je er zeker van dat je dit wil doen? Het kan niet ongedaan gemaakt worden.';
$string['deletetopicvariable'] = 'Verwijder discussie \'%s\'';
$string['editpost'] = 'Bewerk bericht';
$string['editpostsuccess'] = 'Bericht bewerken gelukt';
$string['editstothispost'] = 'Bewerkingen aan dit bericht:';
$string['edittitle'] = 'Bewerk forum';
$string['edittopic'] = 'Bewerk discussie';
$string['edittopicsuccess'] = 'Discussie bewerken gelukt';
$string['forumname'] = 'Forumnaam';
$string['forumsuccessfulsubscribe'] = 'Foruminschrijving gelukt';
$string['forumsuccessfulunsubscribe'] = 'Forum uitschrijving gelukt';
$string['gotoforums'] = 'Ga naar forums';
$string['groupadminlist'] = 'Groepsbeheerders:';
$string['groupadmins'] = 'Groepsbeheerders:';
$string['groupowner'] = 'Groepseigenaar';
$string['groupownerlist'] = 'Groepseigenaar:';
$string['lastpost'] = 'Laatste bericht';
$string['latestforumposts'] = 'Laatste forumberichten';
$string['moderatorsdescription'] = 'Moderators zijn bevoegd discussies en berichten te bewerken en te verwijderen. Zij kunnen ook het forum openen, sluiten en discussies instellen als "sticky"';
$string['moderatorslist'] = 'Moderators:';
$string['name'] = 'Forum';
$string['nameplural'] = 'Forums';
$string['newforum'] = 'Nieuw forum';
$string['newforumpostby'] = '%s: %s: nieuwe berichten gepost door %s';
$string['newforumpostin'] = 'Nieuw forumbericht in %s';
$string['newpost'] = 'Nieuw bericht:';
$string['newtopic'] = 'Nieuwe discussie';
$string['noforumpostsyet'] = 'Er zijn nog geen berichten in deze groep';
$string['noforums'] = 'Er zijn geen forums binnen deze groep';
$string['notopics'] = 'Er zijn geen discussies in dit forum';
$string['orderdescription'] = 'Kies waar je dit forum wil plaatsen ten opzichte van de andere forums';
$string['postbyuserwasdeleted'] = 'Een bericht van %s is verwijderd';
$string['postedin'] = '%s geplaatst in %s';
$string['postreply'] = 'Verstuur reactie';
$string['postsvariable'] = 'Berichten: %s';
$string['potentialmoderators'] = 'Mogelijke Moderatoren';
$string['re'] = 'Re: %s';
$string['regulartopics'] = 'Standaard discussies';
$string['replyto'] = 'Reactie aan:';
$string['replytotopicby'] = '%s: %s: Antwoord aan "%s" door %s';
$string['stickydescription'] = '"Sticky" discussies staan bovenaan elke pagina';
$string['stickytopics'] = '"Sticky" discussies';
$string['strftimerecentfullrelative'] = '%%v, %%l:%%M %%p';
$string['strftimerecentrelative'] = '%%v, %%k:%%M';
$string['subscribetoforum'] = 'Inschrijven op dit forum';
$string['subscribetotopic'] = 'Inschrijven op deze discussie';
$string['today'] = 'Vandaag';
$string['topicclosedsuccess'] = 'Sluiten van discussie gelukt';
$string['topicisclosed'] = 'Deze discussie is gesloten. Alleen de moderator en groeps eigenaar kan een reactie versturen';
$string['topiclower'] = 'discussie';
$string['topicopenedsuccess'] = 'Openen discussie gelukt';
$string['topicslower'] = 'discussies';
$string['topicstickysuccess'] = 'Discussie instellen als sticky gelukt';
$string['topicsubscribesuccess'] = 'Inschrijven op discussie gelukt';
$string['topicunstickysuccess'] = 'Ongedaan maken topic instellen als "sticky" gelukt';
$string['topicunsubscribesuccess'] = 'Discussie uitschrijven gelukt';
$string['topicupdatefailed'] = 'Discussie bijwerken mislukt';
$string['typenewpost'] = 'Nieuw forumbericht';
$string['unsubscribefromforum'] = 'Uitschrijven van forum';
$string['unsubscribefromtopic'] = 'Uitschrijven van discussie';
$string['updateselectedtopics'] = 'Geselecteerde discussies bijwerken';
$string['yesterday'] = 'Gisteren';
?>
